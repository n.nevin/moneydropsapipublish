USE [MoneyDrops]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AboutTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AboutTbl](
	[AboutID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[ShortContent] [nvarchar](max) NULL,
	[LongContent] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[SEOKeywords] [nvarchar](max) NULL,
 CONSTRAINT [PK_About] PRIMARY KEY CLUSTERED 
(
	[AboutID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobApplicationTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobApplicationTbl](
	[CareerJobApplicationID] [uniqueidentifier] NOT NULL,
	[CareerJobOfferID] [uniqueidentifier] NULL,
	[Name] [nvarchar](150) NULL,
	[DOB] [datetime] NULL,
	[Gender] [tinyint] NULL,
	[Qualification] [nvarchar](250) NULL,
	[Comments] [nvarchar](max) NULL,
	[CVFilePath] [nvarchar](max) NULL,
	[ApplicationStatus] [tinyint] NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_CareerJobApplication] PRIMARY KEY CLUSTERED 
(
	[CareerJobApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobOfferRequirementTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobOfferRequirementTbl](
	[CareerJobOfferRequirementID] [uniqueidentifier] NOT NULL,
	[CareerJobOfferID] [uniqueidentifier] NULL,
	[Requirement] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_CareerJobOfferRequirementTbl] PRIMARY KEY CLUSTERED 
(
	[CareerJobOfferRequirementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobOfferTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobOfferTbl](
	[CareerJobOfferID] [uniqueidentifier] NOT NULL,
	[JobTitle] [nvarchar](250) NULL,
	[JobDescription] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[ValidUpto] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_CareerJobOffer] PRIMARY KEY CLUSTERED 
(
	[CareerJobOfferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CityTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CityTbl](
	[CityID] [uniqueidentifier] NOT NULL,
	[CityName] [varchar](50) NOT NULL,
	[StateID] [uniqueidentifier] NOT NULL,
	[CountryID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_CityTbl] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactTbl](
	[ContactID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[DisplaySequence] [int] NOT NULL,
	[AddressLine1] [nvarchar](250) NOT NULL,
	[AddressLine2] [nvarchar](250) NULL,
	[AddressLine3] [nvarchar](250) NULL,
	[CityID] [uniqueidentifier] NOT NULL,
	[StateID] [uniqueidentifier] NOT NULL,
	[CountryID] [uniqueidentifier] NOT NULL,
	[LocLatitude] [nvarchar](max) NULL,
	[LocLongitude] [nvarchar](max) NULL,
	[Email] [nvarchar](250) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Mobile1] [nvarchar](50) NULL,
	[Mobile2] [nvarchar](50) NULL,
	[RecordStatus] [tinyint] NULL,
	[IsHeadOffice] [bit] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CountryTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CountryTbl](
	[CountryID] [uniqueidentifier] NOT NULL,
	[CountryName] [varchar](50) NULL,
 CONSTRAINT [PK_CountryTbl] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FAQTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQTbl](
	[FaqID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [tinyint] NULL,
	[Question] [nvarchar](max) NULL,
	[Answer] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[FaqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NavigationTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NavigationTbl](
	[NavigationID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[Link] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_Navigation] PRIMARY KEY CLUSTERED 
(
	[NavigationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTbl](
	[NewsID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Type] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceCategoryTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceCategoryTbl](
	[ServiceCategoryID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_ServiceCategoryTbl] PRIMARY KEY CLUSTERED 
(
	[ServiceCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceSubCategoryTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceSubCategoryTbl](
	[ServiceSubCategoryID] [uniqueidentifier] NOT NULL,
	[ServiceCategoryID] [uniqueidentifier] NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
 CONSTRAINT [PK_ServiceSubCategory] PRIMARY KEY CLUSTERED 
(
	[ServiceSubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceTbl](
	[ServiceID] [uniqueidentifier] NOT NULL,
	[Type] [tinyint] NULL,
	[ServiceCategoryID] [uniqueidentifier] NULL,
	[ServiceSubCategoryID] [uniqueidentifier] NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_Service_1] PRIMARY KEY CLUSTERED 
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SettingsTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingsTbl](
	[SettingsID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[TagLine] [nvarchar](250) NULL,
	[Logo] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[TermsConditions] [nvarchar](max) NULL,
	[FaviconPath] [nvarchar](max) NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[SettingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SliderTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SliderTbl](
	[SliderID] [int] IDENTITY(1,1) NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_SliderTbl] PRIMARY KEY CLUSTERED 
(
	[SliderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SocialMediaTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SocialMediaTbl](
	[SocialMediaID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Type] [tinyint] NULL,
	[IconPath] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_SocialMedia] PRIMARY KEY CLUSTERED 
(
	[SocialMediaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StateTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StateTbl](
	[StateID] [uniqueidentifier] NOT NULL,
	[StateName] [varchar](50) NULL,
	[CountryID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_StateTbl] PRIMARY KEY CLUSTERED 
(
	[StateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTbl]    Script Date: 23-02-2021 11.06.08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTbl](
	[UserID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](150) NULL,
	[Email] [nvarchar](250) NULL,
	[Username] [nvarchar](150) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Type] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AboutTbl] ON 

INSERT [dbo].[AboutTbl] ([AboutID], [Title], [ImagePath], [ShortContent], [LongContent], [RecordStatus], [CreatedDate], [SEOKeywords]) VALUES (1005, N'AA', N'SDSADSADSA', N'SDSASADSAD', N'dsads', NULL, NULL, N'GYUGUGHJ')
INSERT [dbo].[AboutTbl] ([AboutID], [Title], [ImagePath], [ShortContent], [LongContent], [RecordStatus], [CreatedDate], [SEOKeywords]) VALUES (1006, N'ff', N'ffff', N'fffff', N'ffff', 1, CAST(0x0000ACD400AA83E6 AS DateTime), N'fff')
INSERT [dbo].[AboutTbl] ([AboutID], [Title], [ImagePath], [ShortContent], [LongContent], [RecordStatus], [CreatedDate], [SEOKeywords]) VALUES (1007, N'ff', N'download2021-02-231609PM.png', N'fffff', N'ffff', 1, CAST(0x0000ACD80105CE8D AS DateTime), N'fff')
INSERT [dbo].[AboutTbl] ([AboutID], [Title], [ImagePath], [ShortContent], [LongContent], [RecordStatus], [CreatedDate], [SEOKeywords]) VALUES (1008, N'zz', N'sg2021-02-231611PM.jpg', N'zz', N'zz', 1, CAST(0x0000ACD8010A4E66 AS DateTime), N'zz')
SET IDENTITY_INSERT [dbo].[AboutTbl] OFF
INSERT [dbo].[CareerJobApplicationTbl] ([CareerJobApplicationID], [CareerJobOfferID], [Name], [DOB], [Gender], [Qualification], [Comments], [CVFilePath], [ApplicationStatus], [RecordStatus], [CreatedDate]) VALUES (N'd8284ba0-f508-4263-8e24-0407939aa7a1', N'd1eec6cf-90b1-481b-99d8-00336d62a457', N'dd', CAST(0x0000ACCC00AB1CB0 AS DateTime), 1, N'dd', N'dd', N'dd', 1, NULL, NULL)
INSERT [dbo].[CareerJobApplicationTbl] ([CareerJobApplicationID], [CareerJobOfferID], [Name], [DOB], [Gender], [Qualification], [Comments], [CVFilePath], [ApplicationStatus], [RecordStatus], [CreatedDate]) VALUES (N'd5b47152-70d6-4ef2-a7a4-25a29cb8378f', N'64b2e862-ae38-4780-864b-96bb4c131572', N'App', CAST(0x0000ACCE01250F70 AS DateTime), 0, N'bca', N'sadsa', N'fdsfsa', 0, 0, CAST(0x0000ACD400CAAF3D AS DateTime))
INSERT [dbo].[CareerJobApplicationTbl] ([CareerJobApplicationID], [CareerJobOfferID], [Name], [DOB], [Gender], [Qualification], [Comments], [CVFilePath], [ApplicationStatus], [RecordStatus], [CreatedDate]) VALUES (N'1e1809b8-f2a4-414a-a3c1-e665413ac96d', N'd1eec6cf-90b1-481b-99d8-00336d62a457', N'sds', NULL, 0, N'sdsd', N'sdsd', NULL, 1, 0, CAST(0x0000ACD8011AD62D AS DateTime))
INSERT [dbo].[CareerJobOfferRequirementTbl] ([CareerJobOfferRequirementID], [CareerJobOfferID], [Requirement], [RecordStatus]) VALUES (N'7d5e73e5-8c08-4ed0-92c7-19bd1017bcc3', N'64b2e862-ae38-4780-864b-96bb4c131572', N'.net core , angular', NULL)
INSERT [dbo].[CareerJobOfferTbl] ([CareerJobOfferID], [JobTitle], [JobDescription], [CreatedDate], [ValidUpto], [CreatedBy], [RecordStatus]) VALUES (N'd1eec6cf-90b1-481b-99d8-00336d62a457', N'dwd', N'wdw', CAST(0x0000ACC500697800 AS DateTime), CAST(0x0000ACD4010DB6E0 AS DateTime), NULL, 1)
INSERT [dbo].[CareerJobOfferTbl] ([CareerJobOfferID], [JobTitle], [JobDescription], [CreatedDate], [ValidUpto], [CreatedBy], [RecordStatus]) VALUES (N'64b2e862-ae38-4780-864b-96bb4c131572', N'Software developer', N'C#', CAST(0x0000ACD4009780A0 AS DateTime), CAST(0x0000ACD500F285F0 AS DateTime), NULL, 0)
INSERT [dbo].[CityTbl] ([CityID], [CityName], [StateID], [CountryID]) VALUES (N'9a0b5059-bed9-4484-852a-01de50843c22', N'c city', N'cdd08b45-663c-4ee3-ad6a-c92e10f79991', N'fde990af-e745-4707-ae5f-ebcef4c1979f')
INSERT [dbo].[CityTbl] ([CityID], [CityName], [StateID], [CountryID]) VALUES (N'c6fc4015-5b5a-4e56-9834-89e265e6f548', N'ffff', N'4bf5da92-5d65-4e88-99d8-3f812ab281b4', N'65647c2e-bc7d-4124-8c40-6cd4e9afe5d6')
INSERT [dbo].[CityTbl] ([CityID], [CityName], [StateID], [CountryID]) VALUES (N'1d66ec6f-a034-45af-aafe-8ef2c3f530cd', N'c city 3', N'cdd08b45-663c-4ee3-ad6a-c92e10f79991', N'fde990af-e745-4707-ae5f-ebcef4c1979f')
INSERT [dbo].[CityTbl] ([CityID], [CityName], [StateID], [CountryID]) VALUES (N'ee8f69e6-673b-4775-a3cb-c2d7a211a9bd', N'Tvm', N'4bf5da92-5d65-4e88-99d8-3f812ab281b4', N'65647c2e-bc7d-4124-8c40-6cd4e9afe5d6')
INSERT [dbo].[CityTbl] ([CityID], [CityName], [StateID], [CountryID]) VALUES (N'3a2eb42b-98f4-4510-b38d-f735d27bc3ad', N'Kollam', N'4bf5da92-5d65-4e88-99d8-3f812ab281b4', N'65647c2e-bc7d-4124-8c40-6cd4e9afe5d6')
INSERT [dbo].[ContactTbl] ([ContactID], [Name], [DisplaySequence], [AddressLine1], [AddressLine2], [AddressLine3], [CityID], [StateID], [CountryID], [LocLatitude], [LocLongitude], [Email], [Phone1], [Phone2], [Mobile1], [Mobile2], [RecordStatus], [IsHeadOffice]) VALUES (N'8ba0c0bd-ad77-4649-b4e9-3d6910c720c8', N'Nevin', 1, N'dsd', N'sds', N'dsd', N'3a2eb42b-98f4-4510-b38d-f735d27bc3ad', N'4bf5da92-5d65-4e88-99d8-3f812ab281b4', N'65647c2e-bc7d-4124-8c40-6cd4e9afe5d6', N'sdsdsd', N'sdsd', N'sam@sam.com', N'111111111', N'111111111', N'111111111', N'111111111', 1, NULL)
INSERT [dbo].[CountryTbl] ([CountryID], [CountryName]) VALUES (N'65647c2e-bc7d-4124-8c40-6cd4e9afe5d6', N'India')
INSERT [dbo].[CountryTbl] ([CountryID], [CountryName]) VALUES (N'fde990af-e745-4707-ae5f-ebcef4c1979f', N'China')
SET IDENTITY_INSERT [dbo].[FAQTbl] ON 

INSERT [dbo].[FAQTbl] ([FaqID], [Type], [Question], [Answer], [RecordStatus], [CreatedDate]) VALUES (1, 2, N'dsdsad', N'sdsa', 1, CAST(0x0000ACDD01194000 AS DateTime))
SET IDENTITY_INSERT [dbo].[FAQTbl] OFF
INSERT [dbo].[NavigationTbl] ([NavigationID], [Title], [Description], [Link], [RecordStatus]) VALUES (N'402ec8d3-9944-498a-a87c-031a9bf383d9', N'AA', N'aaaa', N'www.aa.com', 1)
INSERT [dbo].[NavigationTbl] ([NavigationID], [Title], [Description], [Link], [RecordStatus]) VALUES (N'8cba247a-3bcf-40a4-bc5d-1e880a499a9d', N'Home', N'grdgds', N'www.Home.com', 1)
INSERT [dbo].[NewsTbl] ([NewsID], [Title], [Description], [ImagePath], [Type], [CreatedDate]) VALUES (N'caef1b1d-53e6-411c-94b6-4922bbcf4ae9', N'cc', N'cc', N'Untitled2021-02-231643PM.jpg', 1, CAST(0x0000ACD800B8E981 AS DateTime))
INSERT [dbo].[NewsTbl] ([NewsID], [Title], [Description], [ImagePath], [Type], [CreatedDate]) VALUES (N'48e8e09d-f161-4448-adea-a723bc6d1d65', N'zz', N'zz', N'sg2021-02-231705PM.jpg', 1, NULL)
INSERT [dbo].[ServiceCategoryTbl] ([ServiceCategoryID], [Title], [Description], [ImagePath], [DisplaySequence], [RecordStatus]) VALUES (N'd2fd6667-22ea-42a2-b2de-70d291adf856', N'CAT 2', N'CAT 22', N'sg.jpg', 1, 0)
INSERT [dbo].[ServiceCategoryTbl] ([ServiceCategoryID], [Title], [Description], [ImagePath], [DisplaySequence], [RecordStatus]) VALUES (N'c907583e-105a-4402-ad35-b53467c0369b', N'CAT 1', N'CATE 1', N'download.png', 1, 1)
INSERT [dbo].[ServiceCategoryTbl] ([ServiceCategoryID], [Title], [Description], [ImagePath], [DisplaySequence], [RecordStatus]) VALUES (N'97b8bebb-6414-4248-b9af-dae3a311c127', N'CAT 3', N'CAT 3aas', N'sg.jpg', 1, 0)
INSERT [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID], [ServiceCategoryID], [Title], [Description], [ImagePath], [DisplaySequence]) VALUES (N'a1b29275-2107-4429-a355-09854f886206', N'c907583e-105a-4402-ad35-b53467c0369b', N'dd', N'grdgds', N'wsdadasd', 2)
INSERT [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID], [ServiceCategoryID], [Title], [Description], [ImagePath], [DisplaySequence]) VALUES (N'b3cb76a4-cc79-4996-9a12-2fb17205490f', N'c907583e-105a-4402-ad35-b53467c0369b', N'SUB CAT1', N'grdgds', N'sAS', 1)
INSERT [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID], [ServiceCategoryID], [Title], [Description], [ImagePath], [DisplaySequence]) VALUES (N'4017d02d-135f-4e21-aef5-48530181ff89', N'97b8bebb-6414-4248-b9af-dae3a311c127', N'SUB CAT3', N'aDSad', N'ADSa', 1)
INSERT [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID], [ServiceCategoryID], [Title], [Description], [ImagePath], [DisplaySequence]) VALUES (N'140fdfc6-7ebd-475b-b036-ab5738c22d74', N'd2fd6667-22ea-42a2-b2de-70d291adf856', N'SUB CAT2', N'DSA', N'aDa', 2)
INSERT [dbo].[ServiceTbl] ([ServiceID], [Type], [ServiceCategoryID], [ServiceSubCategoryID], [Title], [Description], [ImagePath], [CreatedDate], [RecordStatus]) VALUES (N'33d12609-7c3c-4f9e-a36c-68b123839fd3', 1, N'c907583e-105a-4402-ad35-b53467c0369b', N'4017d02d-135f-4e21-aef5-48530181ff89', N'TITLE', N'www', N'sdsds', CAST(0x0000ACD400C1F4D7 AS DateTime), 0)
INSERT [dbo].[ServiceTbl] ([ServiceID], [Type], [ServiceCategoryID], [ServiceSubCategoryID], [Title], [Description], [ImagePath], [CreatedDate], [RecordStatus]) VALUES (N'f9c9d453-74e6-4fa7-94ea-c157bb909efd', 1, N'97b8bebb-6414-4248-b9af-dae3a311c127', N'a1b29275-2107-4429-a355-09854f886206', N'AA', N'dfsds', N'dsd', NULL, NULL)
INSERT [dbo].[ServiceTbl] ([ServiceID], [Type], [ServiceCategoryID], [ServiceSubCategoryID], [Title], [Description], [ImagePath], [CreatedDate], [RecordStatus]) VALUES (N'24763315-8f3e-451e-8584-e18e9ee5fca2', 1, N'97b8bebb-6414-4248-b9af-dae3a311c127', N'140fdfc6-7ebd-475b-b036-ab5738c22d74', N'TITLE', N'aaaa', N'asas', CAST(0x0000ACD400B4C41C AS DateTime), 0)
INSERT [dbo].[ServiceTbl] ([ServiceID], [Type], [ServiceCategoryID], [ServiceSubCategoryID], [Title], [Description], [ImagePath], [CreatedDate], [RecordStatus]) VALUES (N'32de8483-455b-48a1-a1a2-f808b204a65f', 1, N'97b8bebb-6414-4248-b9af-dae3a311c127', N'4017d02d-135f-4e21-aef5-48530181ff89', N'servi 3', N'aaaa', N'asas', CAST(0x0000ACD800485DDA AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[SettingsTbl] ON 

INSERT [dbo].[SettingsTbl] ([SettingsID], [Title], [TagLine], [Logo], [Description], [TermsConditions], [FaviconPath]) VALUES (6, N'TITLE', N'zz', N'Untitled2021-02-231839PM.jpg', N'aaaa', N'zz', N'download2021-02-231839PM.png')
SET IDENTITY_INSERT [dbo].[SettingsTbl] OFF
SET IDENTITY_INSERT [dbo].[SliderTbl] ON 

INSERT [dbo].[SliderTbl] ([SliderID], [ImagePath], [Title], [Description], [DisplaySequence], [RecordStatus], [CreatedDate]) VALUES (2, N'sg2021-02-232141PM.jpg', N'AA', N'grdgds', 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[SliderTbl] OFF
SET IDENTITY_INSERT [dbo].[SocialMediaTbl] ON 

INSERT [dbo].[SocialMediaTbl] ([SocialMediaID], [Title], [Type], [IconPath], [Link], [DisplaySequence], [RecordStatus]) VALUES (5, N'dsd', 1, N'sg2021-02-232208PM.jpg', N'sdsd', 1, NULL)
SET IDENTITY_INSERT [dbo].[SocialMediaTbl] OFF
INSERT [dbo].[StateTbl] ([StateID], [StateName], [CountryID]) VALUES (N'4bf5da92-5d65-4e88-99d8-3f812ab281b4', N'Kerala', N'65647c2e-bc7d-4124-8c40-6cd4e9afe5d6')
INSERT [dbo].[StateTbl] ([StateID], [StateName], [CountryID]) VALUES (N'cdd08b45-663c-4ee3-ad6a-c92e10f79991', N'c state', N'fde990af-e745-4707-ae5f-ebcef4c1979f')
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'0f8ab020-e649-4f97-b19e-037997adb956', N'Nevin N', N'nevinnepolien@gmail.com', N'nevin', N'Ymq8HNI8zsTpA4pLKSiH05tA1TknzqmTwyPrwydKpsSTx4fJobxHInF91NZ7lYls6zaVtP+UyD6cWphOqOeB5RGNm+jz4ej2Sf7cTJ2Dhk9qwFm5Hqotk1FTm0G2OMM8RNZGMiFVcMOblug9rOTMTn642rxaNPwYiD3cKFhi8v4=', N'3ac94cac-f30b-49a1-b314-2df355c661c0', 1, CAST(0x0000ACD101164BE5 AS DateTime), 1)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'7813e945-0829-44b3-ae38-31c01528d020', N'jd', N'jd@jd.com', N'jd', NULL, NULL, 2, CAST(0x0000ACD1010851B7 AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'cac09abf-e704-4e2c-98e1-37465ff1f508', N'Nevin', N'nevin@gmail.com', N'nevin', N'ifGFzoY5aI72Q6ScUHJmJQv1f7USx1cPYKdwClb9EC2bc7gjrvme2LEQ5u98RMazR9i3lMdm1CfYCSRJw+Sxh937LQQmIQF41g1xhgXFtbOP/p5yNDjZpD+brti1PgsGJHaOZ9W7znMcd9QMwI09ioi/YIWwSGx3OQ0trU4Tc0Q=', N'7f79dc27-4d82-4df2-aec1-4997dc30e9f9', 1, CAST(0x0000ACD000FD10CB AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'67343d27-d9cb-4a4c-97a8-3b70cac17caa', N'Riyas', N'riyas@ri.com', N'riyas', N'SlOZHSC3bzI2OnabOctSlkdqSwb9kfODFSvUgkAreECPG9GzLeqK4VYsmRUkEokGPDmHHXF6sUm2KiE4sl1rc4pJkRgr2Zzsn0GIAOKn5sz6XRTSnpveC5VAmxwMPbLvAXvHf06CqjWjU+qclf+3Hjo/dsXdomydof6NEw8Q+tY=', N'17125778-aaf7-408d-9b7c-8d68347064b6', 1, CAST(0x0000ACD300E84D86 AS DateTime), 1)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'6884556f-a584-4d80-99bd-5ab3a640e9f6', N'User U', N'user@user.com', N'user', NULL, NULL, 2, CAST(0x0000ACCF01323E70 AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'a81211cb-3499-48f1-a4cb-5f813efc5440', N'Sam', N'sam@sam.com', N'sam', N'OmEdyaXred4QekE9lNak3Ls6eUecqItOsp0q7cdTZJhCcOCEm1M5tmHNtRwQJrnjwV5vNRXYkKs/CRtr6/ecfY01D/Z4ZoPx97wRFH0JPL0LXKMsoDl94Vr5biyNqMSe/1K86D5AlS0aREN+kIfYGig3PeIAr2bm5QhAyLvmtxQ=', N'f610cc7c-eaf0-4459-a51b-f6b8233ad9a7', 1, CAST(0x0000ACD0011912E0 AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'1686c655-9774-47b6-9a13-65b411c07e6b', N'dq', N'dq', N'dq', N'dq', NULL, 1, CAST(0x0000ACD10109291C AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'011a168e-5b92-445e-9a11-6edd66878cdb', N'njan', N'njan', N'njan', N'Ur+wbzoIR+aVczUnAa0IJzqOIJsRpgJioOUVBDPr2/ZOxxM434HjA3bIZPcpvMDEQhW8Ne/QoVMVaSDOZYQDx9SCcm8AwsJ2mmaZkThUlXn0OTx7VX8O0PE8zyIjVtbT6AEELHBgt5U33m2B4C7O8M8yI18wac7W7BmGPG5UBH4=', N'25a82cc0-2115-4905-8978-e70f1c446067', 2, CAST(0x0000ACD1010E3376 AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'ac80f160-ab17-42bf-bde4-8a5963e85f53', NULL, NULL, N'nevin', N'9R066BxwMrxr2elPnp2Nipk7pqU5Cs8ITTo5eFvcv/GpqsllS43Y4AjJSniUAwQoAQrWH44vnq0kdUGqtNn4ULt/jQbksxUhPJR+NXRNveUqu7NG8zlqsjhmrNAzSPwemBt8XCyetpasmHbIza/epGOMpKxKoDa+XYkmKYgce1U=', N'cfb01874-43b8-467c-89ae-bd554349a0da', NULL, CAST(0x0000ACD100FC497B AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'b6fc6ed5-0195-4496-bff5-b7f33f477510', N'Minnu', N'minn@ghg.com', N'nev', NULL, NULL, 1, CAST(0x0000ACD1010663C7 AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'593ac48a-a85e-4418-a098-c14c5abaab80', N'Minnu', N'minn@ghg.com', N'minnu', NULL, NULL, 2, CAST(0x0000ACD201323E70 AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'e40066ff-78b5-49f5-a8e0-dd0052d0e6d3', N'kq', N'kq', N'kq', N'kg', NULL, 2, CAST(0x0000ACD10109B2EC AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'db1ea114-96be-454f-b509-ddb503bdc865', N'hainew', N'hai@hai.com', N'hainew', N'bDXUUkn9Jn+LhUYMmdyEx6Sd0VlfiZGrOiLKgEEmi62FsC5MMald3Jg5BXUJAOrwiA5hTzWWpcN3M56ZMl0t941cKZyoSGVNrWBz84Cuu2WjaX5NgPNagMtia3hIUYX9eLecLdwe7ejLwsZFUro6VlYutIF9uwWa7GxYROsIOj4=', N'9fb98b42-d98b-4aae-9589-11310b959933', 1, CAST(0x0000ACD1010DE9C3 AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'127daf5e-90b5-4422-a5ce-e0b5030dbcee', N'Nevin', N'nevin@gmail.com', N'nevin', N'F6swbCpWe6K5G78eaiitKjF9EJ4j9jRYt4qMm4gQFt42UJ0I4Ly7575xlsGZ+VWp0DeYvWUkJubyt8H0St09KFATeZ1WUDjlxKl/8c2hnYiPvGQr/BBA10AnyC6QzCT1Atgcm0QOIZ9h/lewzCkXXhb3X6v5NO/Loe4qaDk29Qw=', N'0eaa1788-0a43-43cf-b0cc-939765c5d0af', 1, CAST(0x0000ACD000FA0F5C AS DateTime), 0)
INSERT [dbo].[UserTbl] ([UserID], [Name], [Email], [Username], [PasswordHash], [PasswordSalt], [Type], [CreatedDate], [RecordStatus]) VALUES (N'feacb824-6eab-42d9-a630-faff7fd997b9', N'Das', N'dd@dd.com', N'das', NULL, NULL, 1, CAST(0x0000ACD0012DF43F AS DateTime), 0)
ALTER TABLE [dbo].[CareerJobApplicationTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobApplicationTbl_CareerJobOfferTbl] FOREIGN KEY([CareerJobOfferID])
REFERENCES [dbo].[CareerJobOfferTbl] ([CareerJobOfferID])
GO
ALTER TABLE [dbo].[CareerJobApplicationTbl] CHECK CONSTRAINT [FK_CareerJobApplicationTbl_CareerJobOfferTbl]
GO
ALTER TABLE [dbo].[CareerJobOfferRequirementTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobOfferRequirementTbl_CareerJobOfferTbl] FOREIGN KEY([CareerJobOfferID])
REFERENCES [dbo].[CareerJobOfferTbl] ([CareerJobOfferID])
GO
ALTER TABLE [dbo].[CareerJobOfferRequirementTbl] CHECK CONSTRAINT [FK_CareerJobOfferRequirementTbl_CareerJobOfferTbl]
GO
ALTER TABLE [dbo].[CareerJobOfferTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobOfferTbl_UserTbl] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTbl] ([UserID])
GO
ALTER TABLE [dbo].[CareerJobOfferTbl] CHECK CONSTRAINT [FK_CareerJobOfferTbl_UserTbl]
GO
ALTER TABLE [dbo].[CityTbl]  WITH CHECK ADD  CONSTRAINT [FK_CityTbl_CountryTbl] FOREIGN KEY([CountryID])
REFERENCES [dbo].[CountryTbl] ([CountryID])
GO
ALTER TABLE [dbo].[CityTbl] CHECK CONSTRAINT [FK_CityTbl_CountryTbl]
GO
ALTER TABLE [dbo].[CityTbl]  WITH CHECK ADD  CONSTRAINT [FK_CityTbl_StateTbl] FOREIGN KEY([StateID])
REFERENCES [dbo].[StateTbl] ([StateID])
GO
ALTER TABLE [dbo].[CityTbl] CHECK CONSTRAINT [FK_CityTbl_StateTbl]
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceCategoryTbl] FOREIGN KEY([ServiceCategoryID])
REFERENCES [dbo].[ServiceCategoryTbl] ([ServiceCategoryID])
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl] CHECK CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceCategoryTbl]
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceSubCategoryTbl] FOREIGN KEY([ServiceSubCategoryID])
REFERENCES [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID])
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl] CHECK CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceSubCategoryTbl]
GO
ALTER TABLE [dbo].[ServiceTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTbl_ServiceCategoryTbl] FOREIGN KEY([ServiceSubCategoryID])
REFERENCES [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID])
GO
ALTER TABLE [dbo].[ServiceTbl] CHECK CONSTRAINT [FK_ServiceTbl_ServiceCategoryTbl]
GO
ALTER TABLE [dbo].[ServiceTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTbl_ServiceSubCategoryTbl] FOREIGN KEY([ServiceSubCategoryID])
REFERENCES [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID])
GO
ALTER TABLE [dbo].[ServiceTbl] CHECK CONSTRAINT [FK_ServiceTbl_ServiceSubCategoryTbl]
GO
ALTER TABLE [dbo].[StateTbl]  WITH CHECK ADD  CONSTRAINT [FK_StateTbl_CountryTbl] FOREIGN KEY([CountryID])
REFERENCES [dbo].[CountryTbl] ([CountryID])
GO
ALTER TABLE [dbo].[StateTbl] CHECK CONSTRAINT [FK_StateTbl_CountryTbl]
GO
