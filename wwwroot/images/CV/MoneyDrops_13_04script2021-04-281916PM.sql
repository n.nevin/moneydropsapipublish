USE [MoneyDrops]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AboutTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AboutTbl](
	[AboutID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[ShortContent] [nvarchar](max) NULL,
	[LongContent] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[SEOKeywords] [nvarchar](max) NULL,
 CONSTRAINT [PK_AboutTbl] PRIMARY KEY CLUSTERED 
(
	[AboutID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AdTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdTbl](
	[AdID] [uniqueidentifier] NOT NULL,
	[Title] [varchar](250) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
 CONSTRAINT [PK_AdTbl] PRIMARY KEY CLUSTERED 
(
	[AdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CareerJobApplicationTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobApplicationTbl](
	[CareerJobApplicationID] [uniqueidentifier] NOT NULL,
	[CareerJobOfferID] [uniqueidentifier] NULL,
	[Name] [nvarchar](150) NULL,
	[DOB] [datetime] NULL,
	[Gender] [tinyint] NULL,
	[Qualification] [nvarchar](250) NULL,
	[Comments] [nvarchar](max) NULL,
	[CVFilePath] [nvarchar](max) NULL,
	[ApplicationStatus] [tinyint] NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_CareerJobApplication] PRIMARY KEY CLUSTERED 
(
	[CareerJobApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobOfferRequirementTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobOfferRequirementTbl](
	[CareerJobOfferRequirementID] [uniqueidentifier] NOT NULL,
	[CareerJobOfferID] [uniqueidentifier] NULL,
	[Requirement] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_CareerJobOfferRequirementTbl] PRIMARY KEY CLUSTERED 
(
	[CareerJobOfferRequirementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobOfferTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobOfferTbl](
	[CareerJobOfferID] [uniqueidentifier] NOT NULL,
	[JobTitle] [nvarchar](250) NULL,
	[JobDescription] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[ValidUpto] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_CareerJobOffer] PRIMARY KEY CLUSTERED 
(
	[CareerJobOfferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CityTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CityTbl](
	[CityID] [uniqueidentifier] NOT NULL,
	[CityName] [varchar](50) NOT NULL,
	[StateID] [uniqueidentifier] NOT NULL,
	[CountryID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_CityTbl] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactTbl](
	[ContactID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[DisplaySequence] [int] NOT NULL,
	[AddressLine1] [nvarchar](250) NOT NULL,
	[AddressLine2] [nvarchar](250) NULL,
	[AddressLine3] [nvarchar](250) NULL,
	[CityID] [uniqueidentifier] NOT NULL,
	[StateID] [uniqueidentifier] NOT NULL,
	[CountryID] [uniqueidentifier] NOT NULL,
	[LocLatitude] [nvarchar](max) NULL,
	[LocLongitude] [nvarchar](max) NULL,
	[Email] [nvarchar](250) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Mobile1] [nvarchar](50) NULL,
	[Mobile2] [nvarchar](50) NULL,
	[RecordStatus] [tinyint] NULL,
	[IsHeadOffice] [bit] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CountryTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CountryTbl](
	[CountryID] [uniqueidentifier] NOT NULL,
	[CountryName] [varchar](50) NULL,
 CONSTRAINT [PK_CountryTbl] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FAQTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQTbl](
	[FaqID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [tinyint] NULL,
	[Question] [nvarchar](max) NULL,
	[Answer] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[FaqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FeaturesTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeaturesTbl](
	[FeatureID] [uniqueidentifier] NOT NULL,
	[ServiceID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_FeaturesTbl] PRIMARY KEY CLUSTERED 
(
	[FeatureID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LiveChatBoxTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiveChatBoxTbl](
	[LiveChatID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NULL,
	[ScriptText] [nchar](250) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_LiveChatBoxTbl] PRIMARY KEY CLUSTERED 
(
	[LiveChatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NavigationTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NavigationTbl](
	[NavigationID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[Link] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_Navigation] PRIMARY KEY CLUSTERED 
(
	[NavigationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTbl](
	[NewsID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Type] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuickApplyNowTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuickApplyNowTbl](
	[QuickApplyNowID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](150) NULL,
	[TypeOfLoan] [tinyint] NULL,
	[Phone] [nchar](50) NULL,
	[Messege] [nvarchar](50) NULL,
	[IsRespond] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_QuickApplyNowTbl] PRIMARY KEY CLUSTERED 
(
	[QuickApplyNowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReferAFriendTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReferAFriendTbl](
	[ReferAFriendID] [uniqueidentifier] NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[FriendName] [nvarchar](250) NULL,
	[FriendEmail] [nvarchar](150) NULL,
	[Messege] [nvarchar](250) NULL,
 CONSTRAINT [PK_ReferAFriendTbl] PRIMARY KEY CLUSTERED 
(
	[ReferAFriendID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RequirementsTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequirementsTbl](
	[RequirementID] [uniqueidentifier] NOT NULL,
	[ServiceID] [uniqueidentifier] NOT NULL,
	[Type] [tinyint] NULL,
	[Title] [nchar](250) NULL,
	[Text] [nchar](250) NULL,
	[Requirements] [nvarchar](max) NULL,
 CONSTRAINT [PK_RequirementsTbl] PRIMARY KEY CLUSTERED 
(
	[RequirementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceCategoryTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceCategoryTbl](
	[ServiceCategoryID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_ServiceCategoryTbl] PRIMARY KEY CLUSTERED 
(
	[ServiceCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceSubCategoryTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceSubCategoryTbl](
	[ServiceSubCategoryID] [uniqueidentifier] NOT NULL,
	[ServiceCategoryID] [uniqueidentifier] NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
 CONSTRAINT [PK_ServiceSubCategory] PRIMARY KEY CLUSTERED 
(
	[ServiceSubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceTbl](
	[ServiceID] [uniqueidentifier] NOT NULL,
	[Type] [tinyint] NULL,
	[ServiceCategoryID] [uniqueidentifier] NULL,
	[ServiceSubCategoryID] [uniqueidentifier] NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_Service_1] PRIMARY KEY CLUSTERED 
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SettingsTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingsTbl](
	[SettingsID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[TagLine] [nvarchar](250) NULL,
	[Logo] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[TermsConditions] [nvarchar](max) NULL,
	[FaviconPath] [nvarchar](max) NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[SettingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SliderTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SliderTbl](
	[SliderID] [int] IDENTITY(1,1) NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_SliderTbl] PRIMARY KEY CLUSTERED 
(
	[SliderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SocialMediaTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SocialMediaTbl](
	[SocialMediaID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Type] [tinyint] NULL,
	[IconPath] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_SocialMedia] PRIMARY KEY CLUSTERED 
(
	[SocialMediaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StateTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StateTbl](
	[StateID] [uniqueidentifier] NOT NULL,
	[StateName] [varchar](50) NULL,
	[CountryID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_StateTbl] PRIMARY KEY CLUSTERED 
(
	[StateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTbl]    Script Date: 13-04-2021 5.14.17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTbl](
	[UserID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](150) NULL,
	[Email] [nvarchar](250) NULL,
	[Username] [nvarchar](150) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Type] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[CareerJobApplicationTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobApplicationTbl_CareerJobOfferTbl] FOREIGN KEY([CareerJobOfferID])
REFERENCES [dbo].[CareerJobOfferTbl] ([CareerJobOfferID])
GO
ALTER TABLE [dbo].[CareerJobApplicationTbl] CHECK CONSTRAINT [FK_CareerJobApplicationTbl_CareerJobOfferTbl]
GO
ALTER TABLE [dbo].[CareerJobOfferRequirementTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobOfferRequirementTbl_CareerJobOfferTbl] FOREIGN KEY([CareerJobOfferID])
REFERENCES [dbo].[CareerJobOfferTbl] ([CareerJobOfferID])
GO
ALTER TABLE [dbo].[CareerJobOfferRequirementTbl] CHECK CONSTRAINT [FK_CareerJobOfferRequirementTbl_CareerJobOfferTbl]
GO
ALTER TABLE [dbo].[CareerJobOfferTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobOfferTbl_UserTbl] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTbl] ([UserID])
GO
ALTER TABLE [dbo].[CareerJobOfferTbl] CHECK CONSTRAINT [FK_CareerJobOfferTbl_UserTbl]
GO
ALTER TABLE [dbo].[CityTbl]  WITH CHECK ADD  CONSTRAINT [FK_CityTbl_CountryTbl] FOREIGN KEY([CountryID])
REFERENCES [dbo].[CountryTbl] ([CountryID])
GO
ALTER TABLE [dbo].[CityTbl] CHECK CONSTRAINT [FK_CityTbl_CountryTbl]
GO
ALTER TABLE [dbo].[CityTbl]  WITH CHECK ADD  CONSTRAINT [FK_CityTbl_StateTbl] FOREIGN KEY([StateID])
REFERENCES [dbo].[StateTbl] ([StateID])
GO
ALTER TABLE [dbo].[CityTbl] CHECK CONSTRAINT [FK_CityTbl_StateTbl]
GO
ALTER TABLE [dbo].[ContactTbl]  WITH CHECK ADD  CONSTRAINT [FK_ContactTbl_CityTbl] FOREIGN KEY([CityID])
REFERENCES [dbo].[CityTbl] ([CityID])
GO
ALTER TABLE [dbo].[ContactTbl] CHECK CONSTRAINT [FK_ContactTbl_CityTbl]
GO
ALTER TABLE [dbo].[ContactTbl]  WITH CHECK ADD  CONSTRAINT [FK_ContactTbl_CountryTbl] FOREIGN KEY([CountryID])
REFERENCES [dbo].[CountryTbl] ([CountryID])
GO
ALTER TABLE [dbo].[ContactTbl] CHECK CONSTRAINT [FK_ContactTbl_CountryTbl]
GO
ALTER TABLE [dbo].[ContactTbl]  WITH CHECK ADD  CONSTRAINT [FK_ContactTbl_StateTbl] FOREIGN KEY([StateID])
REFERENCES [dbo].[StateTbl] ([StateID])
GO
ALTER TABLE [dbo].[ContactTbl] CHECK CONSTRAINT [FK_ContactTbl_StateTbl]
GO
ALTER TABLE [dbo].[FeaturesTbl]  WITH CHECK ADD  CONSTRAINT [FK_FeaturesTbl_ServiceTbl] FOREIGN KEY([ServiceID])
REFERENCES [dbo].[ServiceTbl] ([ServiceID])
GO
ALTER TABLE [dbo].[FeaturesTbl] CHECK CONSTRAINT [FK_FeaturesTbl_ServiceTbl]
GO
ALTER TABLE [dbo].[ReferAFriendTbl]  WITH CHECK ADD  CONSTRAINT [FK_ReferAFriendTbl_UserTbl] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTbl] ([UserID])
GO
ALTER TABLE [dbo].[ReferAFriendTbl] CHECK CONSTRAINT [FK_ReferAFriendTbl_UserTbl]
GO
ALTER TABLE [dbo].[RequirementsTbl]  WITH CHECK ADD  CONSTRAINT [FK_RequirementsTbl_ServiceTbl] FOREIGN KEY([ServiceID])
REFERENCES [dbo].[ServiceTbl] ([ServiceID])
GO
ALTER TABLE [dbo].[RequirementsTbl] CHECK CONSTRAINT [FK_RequirementsTbl_ServiceTbl]
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceCategoryTbl] FOREIGN KEY([ServiceCategoryID])
REFERENCES [dbo].[ServiceCategoryTbl] ([ServiceCategoryID])
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl] CHECK CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceCategoryTbl]
GO
ALTER TABLE [dbo].[ServiceTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTbl_ServiceCategoryTbl] FOREIGN KEY([ServiceSubCategoryID])
REFERENCES [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID])
GO
ALTER TABLE [dbo].[ServiceTbl] CHECK CONSTRAINT [FK_ServiceTbl_ServiceCategoryTbl]
GO
ALTER TABLE [dbo].[StateTbl]  WITH CHECK ADD  CONSTRAINT [FK_StateTbl_CountryTbl] FOREIGN KEY([CountryID])
REFERENCES [dbo].[CountryTbl] ([CountryID])
GO
ALTER TABLE [dbo].[StateTbl] CHECK CONSTRAINT [FK_StateTbl_CountryTbl]
GO
