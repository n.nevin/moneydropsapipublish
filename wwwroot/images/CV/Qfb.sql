USE [master]
GO
/****** Object:  Database [QuestForBest]    Script Date: 26-03-2021 09:57:40 ******/
CREATE DATABASE [QuestForBest]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuestForBest', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.ASPIROM\MSSQL\DATA\QuestForBest.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QuestForBest_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.ASPIROM\MSSQL\DATA\QuestForBest_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QuestForBest] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuestForBest].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuestForBest] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuestForBest] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuestForBest] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuestForBest] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuestForBest] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuestForBest] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuestForBest] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [QuestForBest] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuestForBest] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuestForBest] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuestForBest] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuestForBest] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuestForBest] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuestForBest] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuestForBest] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuestForBest] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QuestForBest] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuestForBest] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuestForBest] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuestForBest] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuestForBest] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuestForBest] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuestForBest] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuestForBest] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QuestForBest] SET  MULTI_USER 
GO
ALTER DATABASE [QuestForBest] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuestForBest] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuestForBest] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuestForBest] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [QuestForBest]
GO
/****** Object:  Table [dbo].[Advertisement]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Advertisement](
	[AdsID] [int] IDENTITY(1,1) NOT NULL,
	[Tittle] [nvarchar](50) NOT NULL,
	[SecondTitle] [nvarchar](50) NOT NULL,
	[Image] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ShortDescription] [nvarchar](50) NOT NULL,
	[AdvertismentPositionID] [int] NOT NULL,
	[Entredon] [date] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_Adverisment] PRIMARY KEY CLUSTERED 
(
	[AdsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AdvertisementPosition]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvertisementPosition](
	[AdvertismentPositionID] [int] IDENTITY(1,1) NOT NULL,
	[PageTitle] [nvarchar](50) NOT NULL,
	[Position] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AdverismentPosition] PRIMARY KEY CLUSTERED 
(
	[AdvertismentPositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AdvertisementShowing]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvertisementShowing](
	[AdsShowID] [int] IDENTITY(1,1) NOT NULL,
	[AdsID] [int] NOT NULL,
	[ExpieredOn] [date] NOT NULL,
 CONSTRAINT [PK_AdvertismentShowing] PRIMARY KEY CLUSTERED 
(
	[AdsShowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Company]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](350) NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](15) NULL,
	[LicenceType] [tinyint] NOT NULL,
	[DeviceLimit] [int] NULL,
	[ImagePath] [nvarchar](100) NULL,
	[MailStatus] [bit] NOT NULL,
	[RecordStatus] [tinyint] NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CouponType]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponType](
	[CouponTypeId] [bigint] NOT NULL,
	[MerchantLayerID] [int] NOT NULL,
	[MerchantTypeUID] [uniqueidentifier] NULL,
	[MinValue] [decimal](18, 2) NOT NULL,
	[MaxValue] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_BaseCoupon] PRIMARY KEY CLUSTERED 
(
	[CouponTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[District]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[DistrictID] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[DistrictID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Event]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Image] [nvarchar](50) NOT NULL,
	[EventDate] [date] NOT NULL,
	[Venue] [nvarchar](50) NOT NULL,
	[DistrictID] [int] NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[isActive] [bit] NOT NULL,
	[EventCreatedBy] [nvarchar](50) NOT NULL,
	[SponcerdBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Game]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Game](
	[GameId] [bigint] IDENTITY(1,1) NOT NULL,
	[GameUID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[PublsihOn] [date] NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[Active] [bit] NOT NULL,
	[iSLogin] [bit] NOT NULL,
	[LogID] [int] NOT NULL,
	[SponserdBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[GameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GameQuestion]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameQuestion](
	[GameQuestionId] [bigint] IDENTITY(1,1) NOT NULL,
	[GameQuestionUID] [uniqueidentifier] NOT NULL,
	[GameId] [bigint] NOT NULL,
	[QuestionId] [bigint] NOT NULL,
	[PublishedStatus] [tinyint] NOT NULL,
	[PublishedTime] [datetime] NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_GameQuestion] PRIMARY KEY CLUSTERED 
(
	[GameQuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GameResult]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameResult](
	[GameResultId] [bigint] IDENTITY(1,1) NOT NULL,
	[GameResultUID] [uniqueidentifier] NOT NULL,
	[SubscriberId] [bigint] NOT NULL,
	[GameId] [bigint] NOT NULL,
	[GameQuestionId] [bigint] NOT NULL,
	[QuestionAnswerId] [bigint] NOT NULL,
	[IsCorrectAnswer] [bit] NOT NULL,
	[DisplayTime] [datetime] NOT NULL,
	[ResponseTime] [datetime] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_GameResult] PRIMARY KEY CLUSTERED 
(
	[GameResultId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Log]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[AdsQues] [nvarchar](250) NOT NULL,
	[AdsAns] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchantCoupon]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchantCoupon](
	[MerchantCouponId] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantCouponUID] [uniqueidentifier] NULL,
	[MerchentID] [bigint] NOT NULL,
	[MerchantUID] [uniqueidentifier] NULL,
	[CouponName] [nvarchar](50) NOT NULL,
	[CouponCode] [nvarchar](50) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[ValidOn] [date] NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CouponStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Coupon] PRIMARY KEY CLUSTERED 
(
	[MerchantCouponId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchantLayer]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchantLayer](
	[LayerID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Layer] PRIMARY KEY CLUSTERED 
(
	[LayerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchantType]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchantType](
	[MerchantTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NULL,
	[MerchantTypeUID] [uniqueidentifier] NULL,
	[MerchantCategoryId] [tinyint] NULL,
 CONSTRAINT [PK_MerchantType] PRIMARY KEY CLUSTERED 
(
	[MerchantTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Merchent]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Merchent](
	[MerchentID] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchentUID] [uniqueidentifier] NULL,
	[MerchentDetailsID] [int] NULL,
	[TalukID] [int] NULL,
	[UnitId] [int] NULL,
	[UniqueID] [varchar](50) NOT NULL,
	[LayerID] [int] NOT NULL,
	[MerchentName] [varchar](500) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Email] [nvarchar](150) NULL,
	[MobileNo] [nvarchar](15) NULL,
	[Address] [nvarchar](250) NULL,
	[Pincode] [nvarchar](10) NULL,
	[IsVerify] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[MerchantTypeId] [int] NULL,
	[MerchantTypeUID] [uniqueidentifier] NULL,
	[Latitude] [nvarchar](max) NULL,
	[Longitute] [nvarchar](max) NULL,
	[Landmark] [nvarchar](250) NULL,
	[MerchantStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[DistrictID] [int] NULL,
	[StateId] [int] NULL,
 CONSTRAINT [PK_Merchent] PRIMARY KEY CLUSTERED 
(
	[MerchentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MerchentCouponTransaction]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentCouponTransaction](
	[MerchentSubstransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentTranscationID] [int] NOT NULL,
	[TransactionOn] [date] NOT NULL,
	[Amount] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_MerchentSubscriberTransaction] PRIMARY KEY CLUSTERED 
(
	[MerchentSubstransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchentDetails]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentDetails](
	[MerchentDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[SubCode] [nvarchar](50) NOT NULL,
	[Barcode] [nvarchar](50) NULL,
	[UnitID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MerchentDetails] PRIMARY KEY CLUSTERED 
(
	[MerchentDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchentDocumnt]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentDocumnt](
	[MerchentDocumentID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentDocumentUID] [uniqueidentifier] NULL,
	[MerchentID] [bigint] NOT NULL,
	[DocumentType] [nvarchar](50) NOT NULL,
	[DocumentName] [nvarchar](50) NULL,
	[Path] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_MerchentDocumnt] PRIMARY KEY CLUSTERED 
(
	[MerchentDocumentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchentFundRequest]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MerchentFundRequest](
	[FundRequestID] [int] IDENTITY(1,1) NOT NULL,
	[FundRequestUID] [uniqueidentifier] NULL,
	[MerchentID] [bigint] NULL,
	[TransactionUniqID] [varchar](50) NULL,
	[BankName] [nvarchar](50) NOT NULL,
	[IFSCCode] [varchar](50) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[TransferDate] [date] NOT NULL,
	[IsApprove] [bit] NOT NULL,
	[ApprovedDate] [datetime] NULL,
	[FundRequestStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[ApprovedBy] [int] NULL,
 CONSTRAINT [PK_MerchentFundRequest] PRIMARY KEY CLUSTERED 
(
	[FundRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MerchentSubscription]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentSubscription](
	[MerchentSubscriptionID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
 CONSTRAINT [PK_MerchentSubscription] PRIMARY KEY CLUSTERED 
(
	[MerchentSubscriptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchentTransaction]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentTransaction](
	[MerchentTranscationID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
	[TransactionID] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [tinyint] NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_MerchentTransaction] PRIMARY KEY CLUSTERED 
(
	[MerchentTranscationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchentUnit]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentUnit](
	[MerchentUnitD] [int] IDENTITY(1,1) NOT NULL,
	[TalukID] [int] NULL,
	[UnitName] [nvarchar](50) NULL,
	[Status] [tinyint] NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[MerchentUnitD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchentVoucher]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentVoucher](
	[MerchentVoucherId] [int] IDENTITY(1,1) NOT NULL,
	[MerchentVoucherUID] [uniqueidentifier] NULL,
	[MerchentID] [bigint] NOT NULL,
	[MerchentUID] [uniqueidentifier] NULL,
	[MerchentName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[MobileNo] [nvarchar](15) NOT NULL,
	[MerchantCouponId] [bigint] NOT NULL,
	[MerchantCouponUID] [uniqueidentifier] NULL,
	[ExpiryOn] [date] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsRedeemed] [bit] NOT NULL,
	[MerchantCouponStatus] [tinyint] NULL,
 CONSTRAINT [PK_MerchentVoucher] PRIMARY KEY CLUSTERED 
(
	[MerchentVoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MerchentWallet]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentWallet](
	[MerchentWalletID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_MercehntWallet] PRIMARY KEY CLUSTERED 
(
	[MerchentWalletID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationID] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[UpdatedOn] [date] NOT NULL,
	[EntredOn] [date] NOT NULL,
	[Url] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramDetail]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramDetail](
	[ProgramDetailsId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProgramDetailsUID] [uniqueidentifier] NULL,
	[ProgramMasterId] [bigint] NULL,
	[ProgramMasterUID] [uniqueidentifier] NULL,
	[ProgramDetailName] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ProgramDetail] PRIMARY KEY CLUSTERED 
(
	[ProgramDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramMaster]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramMaster](
	[ProgramMasterId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProgramMasterUID] [uniqueidentifier] NULL,
	[ProgramName] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ProgramMaster] PRIMARY KEY CLUSTERED 
(
	[ProgramMasterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramParticipiant]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramParticipiant](
	[ProgramParticipiantId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProgramParticipiantUID] [uniqueidentifier] NULL,
	[ProgramDetailsId] [bigint] NULL,
	[ProgramDetailsUID] [uniqueidentifier] NULL,
	[TeamId] [bigint] NULL,
	[TeamUID] [uniqueidentifier] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ProgramParticipiant] PRIMARY KEY CLUSTERED 
(
	[ProgramParticipiantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramScore]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramScore](
	[ProgramScoreId] [bigint] NOT NULL,
	[ProgramParticipiantId] [bigint] NULL,
	[ProgramParticipiantUID] [uniqueidentifier] NULL,
	[ProgramMasterUID] [uniqueidentifier] NULL,
	[ProgramName] [nvarchar](50) NULL,
	[ProgramDetailsUID] [uniqueidentifier] NULL,
	[ProgramDetailName] [nvarchar](50) NULL,
	[TeamUID] [uniqueidentifier] NULL,
	[TeamName] [nvarchar](150) NULL,
	[SubscriberId] [bigint] NULL,
	[SubscriberUID] [uniqueidentifier] NULL,
	[SubscriberCode] [nvarchar](15) NULL,
	[SubscriberFirstName] [nvarchar](100) NULL,
	[SubscriberLastName] [nvarchar](100) NULL,
	[SubscriberEmail] [nvarchar](150) NULL,
	[SubscriberMobileNo] [nvarchar](15) NULL,
	[Rate] [int] NULL,
	[EnteredOn] [datetime] NULL,
 CONSTRAINT [PK_ProgramScore] PRIMARY KEY CLUSTERED 
(
	[ProgramScoreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Question]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[QuestionId] [bigint] IDENTITY(1,1) NOT NULL,
	[QuestionUID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Type] [tinyint] NOT NULL,
	[Path] [nvarchar](500) NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[SponserdBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuestionAnswer]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionAnswer](
	[QuestionAnswerId] [bigint] IDENTITY(1,1) NOT NULL,
	[QuestionAnswerUID] [uniqueidentifier] NOT NULL,
	[QuestionId] [bigint] NOT NULL,
	[Choice] [nvarchar](250) NOT NULL,
	[Path] [nvarchar](500) NULL,
	[Type] [tinyint] NOT NULL,
	[IsAnswer] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_QuestionAnswer] PRIMARY KEY CLUSTERED 
(
	[QuestionAnswerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sponser]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sponser](
	[SponserID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Logo] [nvarchar](150) NOT NULL,
	[Text] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
	[ShortDescription] [nvarchar](550) NOT NULL,
 CONSTRAINT [PK_Sponser] PRIMARY KEY CLUSTERED 
(
	[SponserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[State]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subscriber]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subscriber](
	[SubscriberId] [bigint] IDENTITY(1,1) NOT NULL,
	[SubscriberUID] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](15) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Username] [nvarchar](150) NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Email] [nvarchar](150) NULL,
	[MobileNo] [nvarchar](15) NOT NULL,
	[AadhaarNo] [nvarchar](30) NULL,
	[Address] [nvarchar](250) NULL,
	[Pincode] [nvarchar](10) NULL,
	[TalukID] [int] NULL,
	[SubscriberStatus] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[SubsUniqueID] [int] NULL,
	[EmailVerifictaionCode] [nvarchar](50) NULL,
	[EmailIsVerify] [bit] NULL,
	[PhoneVerificationcode] [nvarchar](50) NULL,
	[PhoneIsVerify] [bit] NULL,
	[PasswordResetCode] [nvarchar](50) NULL,
	[ProfilePic] [nvarchar](50) NULL,
	[SubsDetailsID] [int] NULL,
	[isVerify] [bit] NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_Subscriber] PRIMARY KEY CLUSTERED 
(
	[SubscriberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubscriberDetails]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberDetails](
	[SubsDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[SubsCode] [int] NOT NULL,
	[SetBoxBarCode] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](250) NOT NULL,
	[MobileNo] [nvarchar](20) NOT NULL,
	[District] [nvarchar](50) NOT NULL,
	[OperatorCode] [int] NOT NULL,
 CONSTRAINT [PK_SubscriberDetails] PRIMARY KEY CLUSTERED 
(
	[SubsDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubscriberVoucher]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberVoucher](
	[SubscriberVoucherId] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberVoucherUID] [uniqueidentifier] NULL,
	[SubscriberID] [bigint] NOT NULL,
	[SubscriberUID] [uniqueidentifier] NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[MobileNo] [nvarchar](15) NOT NULL,
	[MerchantCouponId] [bigint] NOT NULL,
	[MerchantCouponUID] [uniqueidentifier] NULL,
	[ExpiryOn] [date] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsRedeemed] [bit] NOT NULL,
	[SubsciberCouponStatus] [tinyint] NULL,
 CONSTRAINT [PK_SubscriberVoucher] PRIMARY KEY CLUSTERED 
(
	[SubscriberVoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubscriberWallet]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberWallet](
	[walletID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberId] [bigint] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_Wallet] PRIMARY KEY CLUSTERED 
(
	[walletID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubscriptionPackage]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriptionPackage](
	[SubscriptionPackageID] [int] IDENTITY(1,1) NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[SubscriptionPackageName] [nvarchar](50) NOT NULL,
	[Validity] [date] NOT NULL,
	[Amount] [int] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_SubscriptionPackage] PRIMARY KEY CLUSTERED 
(
	[SubscriptionPackageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Taluk]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Taluk](
	[TalukID] [int] IDENTITY(1,1) NOT NULL,
	[DistrictID] [int] NULL,
	[TalukName] [nvarchar](50) NULL,
	[Status] [tinyint] NULL,
 CONSTRAINT [PK_Taluk] PRIMARY KEY CLUSTERED 
(
	[TalukID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Team]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Team](
	[TeamId] [bigint] IDENTITY(1,1) NOT NULL,
	[TeamUID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[TeamLead] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED 
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberID] [bigint] NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Amount] [int] NOT NULL,
	[UpdtedOn] [date] NOT NULL,
	[CreatedOn] [date] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 26-03-2021 09:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Email] [nvarchar](100) NULL,
	[Status] [tinyint] NOT NULL,
	[RoleType] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ResetCode] [nvarchar](100) NOT NULL,
	[ProfilePic] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Advertisement] ON 

INSERT [dbo].[Advertisement] ([AdsID], [Tittle], [SecondTitle], [Image], [Description], [ShortDescription], [AdvertismentPositionID], [Entredon], [UpdatedOn]) VALUES (1, N'Test Uploaded ', N'asdasdasd', N'logo.png', N'ads', N'ads', 1, CAST(0xC61E0100 AS Date), CAST(0xC61E0100 AS Date))
SET IDENTITY_INSERT [dbo].[Advertisement] OFF
SET IDENTITY_INSERT [dbo].[AdvertisementPosition] ON 

INSERT [dbo].[AdvertisementPosition] ([AdvertismentPositionID], [PageTitle], [Position]) VALUES (1, N'Home Page - Bottom Left', N'BL')
INSERT [dbo].[AdvertisementPosition] ([AdvertismentPositionID], [PageTitle], [Position]) VALUES (2, N'Home Page - Bottom Center', N'BC')
INSERT [dbo].[AdvertisementPosition] ([AdvertismentPositionID], [PageTitle], [Position]) VALUES (3, N'Home Page - Bottom Right', N'BR')
INSERT [dbo].[AdvertisementPosition] ([AdvertismentPositionID], [PageTitle], [Position]) VALUES (4, N'Home Page - Right Top', N'RT')
INSERT [dbo].[AdvertisementPosition] ([AdvertismentPositionID], [PageTitle], [Position]) VALUES (5, N'Home Page - Right Center', N'RC')
INSERT [dbo].[AdvertisementPosition] ([AdvertismentPositionID], [PageTitle], [Position]) VALUES (6, N'Home Page - Right Bottom', N'RB')
SET IDENTITY_INSERT [dbo].[AdvertisementPosition] OFF
SET IDENTITY_INSERT [dbo].[AdvertisementShowing] ON 

INSERT [dbo].[AdvertisementShowing] ([AdsShowID], [AdsID], [ExpieredOn]) VALUES (1, 1, CAST(0x52420B00 AS Date))
INSERT [dbo].[AdvertisementShowing] ([AdsShowID], [AdsID], [ExpieredOn]) VALUES (9, 1, CAST(0x4F420B00 AS Date))
SET IDENTITY_INSERT [dbo].[AdvertisementShowing] OFF
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([CountryId], [Code], [Name], [Status]) VALUES (1, N'IN', N'India', 0)
SET IDENTITY_INSERT [dbo].[Country] OFF
INSERT [dbo].[CouponType] ([CouponTypeId], [MerchantLayerID], [MerchantTypeUID], [MinValue], [MaxValue], [CreatedDate]) VALUES (1, 1, NULL, CAST(5000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[CouponType] ([CouponTypeId], [MerchantLayerID], [MerchantTypeUID], [MinValue], [MaxValue], [CreatedDate]) VALUES (2, 2, NULL, CAST(500.00 AS Decimal(18, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[District] ON 

INSERT [dbo].[District] ([DistrictID], [StateId], [Name], [Status]) VALUES (1, 1, N'Trivandrum', 0)
SET IDENTITY_INSERT [dbo].[District] OFF
SET IDENTITY_INSERT [dbo].[MerchantLayer] ON 

INSERT [dbo].[MerchantLayer] ([LayerID], [Type]) VALUES (1, N'Advertisement')
INSERT [dbo].[MerchantLayer] ([LayerID], [Type]) VALUES (2, N'No Advertisement')
SET IDENTITY_INSERT [dbo].[MerchantLayer] OFF
SET IDENTITY_INSERT [dbo].[MerchantType] ON 

INSERT [dbo].[MerchantType] ([MerchantTypeId], [TypeName], [MerchantTypeUID], [MerchantCategoryId]) VALUES (1, N'wholesale', N'1d3cc6de-cb57-40f8-a86f-681431a68806', 1)
SET IDENTITY_INSERT [dbo].[MerchantType] OFF
SET IDENTITY_INSERT [dbo].[Merchent] ON 

INSERT [dbo].[Merchent] ([MerchentID], [MerchentUID], [MerchentDetailsID], [TalukID], [UnitId], [UniqueID], [LayerID], [MerchentName], [PasswordHash], [PasswordSalt], [Email], [MobileNo], [Address], [Pincode], [IsVerify], [IsActive], [MerchantTypeId], [MerchantTypeUID], [Latitude], [Longitute], [Landmark], [MerchantStatus], [CreatedDate], [CreatedBy], [DistrictID], [StateId]) VALUES (6, N'1d3cc6de-cb57-40f8-a86f-681431a68806', NULL, 1, 1, N'KVM20210002', 1, N'ab', N'oKdNNuagsmG28o7evjjgPP5Nj6zOGs95wtok/aNwPnix9Wi4dhuRU+hD1RCPfQvgXTJF97c3VIlUGYR8QlJ98Q+PjvQJtKmoMVtkXZsp0eoUBv+0Qko199CcOarO4pWGlUbV/8vAaLzL0SLofjYn3BVKM01iuAdUnkobJptGf04=', N'ca1fc996-3328-4e07-b97f-9d664d58901d', N'av@gmail.com', N'1', N'aaa', N'1', 1, 1, 1, N'1d3cc6de-cb57-40f8-a86f-681431a68806', NULL, NULL, NULL, 0, NULL, NULL, 1, 1)
INSERT [dbo].[Merchent] ([MerchentID], [MerchentUID], [MerchentDetailsID], [TalukID], [UnitId], [UniqueID], [LayerID], [MerchentName], [PasswordHash], [PasswordSalt], [Email], [MobileNo], [Address], [Pincode], [IsVerify], [IsActive], [MerchantTypeId], [MerchantTypeUID], [Latitude], [Longitute], [Landmark], [MerchantStatus], [CreatedDate], [CreatedBy], [DistrictID], [StateId]) VALUES (7, N'00000000-0000-0000-0000-000000000000', NULL, 1, NULL, N'KVM20210007', 2, N'e', N'2qdeoh6guTnvAMDDhqfy4h57iLzSWKkqHUF1eaWz471FsJayXZaWCWTQ0rWbSJEW/sUmcEicfSGWdDm2msf3YkOmP+UffcKt4qmvTdIo7LQndiCxO2cV/YoYs0xpc8cyF7duIULruUtci1RvoIqSXohV5U9vkEKKde5/HNTYc58=', N'999cb27b-1889-4089-9844-a15350320343', N'av@gmail.com', N'2', N'a', N'2', 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[Merchent] OFF
SET IDENTITY_INSERT [dbo].[MerchentTransaction] ON 

INSERT [dbo].[MerchentTransaction] ([MerchentTranscationID], [MerchentID], [TransactionID], [Amount], [Status], [CreatedOn], [UpdatedOn]) VALUES (1, 7, 3, CAST(0.00 AS Decimal(18, 2)), 1, CAST(0x52420B00 AS Date), CAST(0x52420B00 AS Date))
SET IDENTITY_INSERT [dbo].[MerchentTransaction] OFF
SET IDENTITY_INSERT [dbo].[MerchentUnit] ON 

INSERT [dbo].[MerchentUnit] ([MerchentUnitD], [TalukID], [UnitName], [Status]) VALUES (1, 1, N'kmn', 0)
SET IDENTITY_INSERT [dbo].[MerchentUnit] OFF
SET IDENTITY_INSERT [dbo].[MerchentWallet] ON 

INSERT [dbo].[MerchentWallet] ([MerchentWalletID], [MerchentID], [Type], [Amount], [UpdatedOn]) VALUES (1, 7, N'FORCOUPON', CAST(0.00 AS Decimal(18, 2)), CAST(0x0000ACF700A1ECE5 AS DateTime))
SET IDENTITY_INSERT [dbo].[MerchentWallet] OFF
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([NotificationID], [Image], [Title], [Description], [UpdatedOn], [EntredOn], [Url]) VALUES (1, N'haiii', N'aaaa', N'aaaa', CAST(0x0D420B00 AS Date), CAST(0x0D420B00 AS Date), N'aaaa')
INSERT [dbo].[Notification] ([NotificationID], [Image], [Title], [Description], [UpdatedOn], [EntredOn], [Url]) VALUES (3, N'ddd', N'ddd', N'dddffff', CAST(0x00000000 AS Date), CAST(0x00000000 AS Date), N'dddddd')
SET IDENTITY_INSERT [dbo].[Notification] OFF
SET IDENTITY_INSERT [dbo].[ProgramDetail] ON 

INSERT [dbo].[ProgramDetail] ([ProgramDetailsId], [ProgramDetailsUID], [ProgramMasterId], [ProgramMasterUID], [ProgramDetailName], [CreatedDate], [CreatedBy]) VALUES (1, NULL, 1, NULL, N'Test program Details Name', NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProgramDetail] OFF
SET IDENTITY_INSERT [dbo].[ProgramMaster] ON 

INSERT [dbo].[ProgramMaster] ([ProgramMasterId], [ProgramMasterUID], [ProgramName], [CreatedDate], [CreatedBy]) VALUES (1, NULL, N'Test Program Name', NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProgramMaster] OFF
SET IDENTITY_INSERT [dbo].[ProgramParticipiant] ON 

INSERT [dbo].[ProgramParticipiant] ([ProgramParticipiantId], [ProgramParticipiantUID], [ProgramDetailsId], [ProgramDetailsUID], [TeamId], [TeamUID], [CreatedDate], [CreatedBy]) VALUES (1, NULL, 1, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[ProgramParticipiant] ([ProgramParticipiantId], [ProgramParticipiantUID], [ProgramDetailsId], [ProgramDetailsUID], [TeamId], [TeamUID], [CreatedDate], [CreatedBy]) VALUES (2, NULL, 1, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[ProgramParticipiant] ([ProgramParticipiantId], [ProgramParticipiantUID], [ProgramDetailsId], [ProgramDetailsUID], [TeamId], [TeamUID], [CreatedDate], [CreatedBy]) VALUES (3, NULL, 1, NULL, 14, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProgramParticipiant] OFF
SET IDENTITY_INSERT [dbo].[Question] ON 

INSERT [dbo].[Question] ([QuestionId], [QuestionUID], [Name], [Type], [Path], [Status], [CreatedDate], [SponserdBy]) VALUES (1, N'e2a4fa47-c0b6-4ab6-9528-085f24e1d566', N'Test', 0, N'testpath', 0, CAST(0x0000ACF300E6B9A8 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Question] OFF
SET IDENTITY_INSERT [dbo].[QuestionAnswer] ON 

INSERT [dbo].[QuestionAnswer] ([QuestionAnswerId], [QuestionAnswerUID], [QuestionId], [Choice], [Path], [Type], [IsAnswer], [Status], [CreatedDate]) VALUES (1, N'69b0fd7f-7baf-4fa2-8ccf-1883a43c4b59', 1, N'A', NULL, 0, 1, 0, CAST(0x0000ACF300E6BA13 AS DateTime))
INSERT [dbo].[QuestionAnswer] ([QuestionAnswerId], [QuestionAnswerUID], [QuestionId], [Choice], [Path], [Type], [IsAnswer], [Status], [CreatedDate]) VALUES (2, N'7114207d-1f88-4f6e-bac4-7458c559c1df', 1, N'B', NULL, 0, 0, 0, CAST(0x0000ACF300E6BA1C AS DateTime))
INSERT [dbo].[QuestionAnswer] ([QuestionAnswerId], [QuestionAnswerUID], [QuestionId], [Choice], [Path], [Type], [IsAnswer], [Status], [CreatedDate]) VALUES (3, N'b4be654f-2ae3-4bd4-a518-effbc9f64ae4', 1, N'C', NULL, 0, 0, 0, CAST(0x0000ACF300E6BA1D AS DateTime))
INSERT [dbo].[QuestionAnswer] ([QuestionAnswerId], [QuestionAnswerUID], [QuestionId], [Choice], [Path], [Type], [IsAnswer], [Status], [CreatedDate]) VALUES (4, N'b6f439ad-43b7-4820-94d6-b0f6eb01691a', 1, N'D', NULL, 0, 0, 0, CAST(0x0000ACF300E6BA1E AS DateTime))
SET IDENTITY_INSERT [dbo].[QuestionAnswer] OFF
SET IDENTITY_INSERT [dbo].[Sponser] ON 

INSERT [dbo].[Sponser] ([SponserID], [Name], [Logo], [Text], [Description], [ShortDescription]) VALUES (4, N'Test', N'asd', N'asd', N'asd', N'ada')
SET IDENTITY_INSERT [dbo].[Sponser] OFF
SET IDENTITY_INSERT [dbo].[State] ON 

INSERT [dbo].[State] ([StateId], [CountryId], [Name], [Code], [Status]) VALUES (1, 1, N'Kerala', N'KL', 0)
SET IDENTITY_INSERT [dbo].[State] OFF
SET IDENTITY_INSERT [dbo].[Subscriber] ON 

INSERT [dbo].[Subscriber] ([SubscriberId], [SubscriberUID], [Code], [FirstName], [LastName], [Username], [PasswordHash], [PasswordSalt], [Email], [MobileNo], [AadhaarNo], [Address], [Pincode], [TalukID], [SubscriberStatus], [CreatedDate], [SubsUniqueID], [EmailVerifictaionCode], [EmailIsVerify], [PhoneVerificationcode], [PhoneIsVerify], [PasswordResetCode], [ProfilePic], [SubsDetailsID], [isVerify], [isActive]) VALUES (1, N'1d3cc6de-cb57-40f8-a86f-681431a68806', N'KV20210001', N'Abdulla', N'Sirajudeen', N'abdu', N'oKdNNuagsmG28o7evjjgPP5Nj6zOGs95wtok/aNwPnix9Wi4dhuRU+hD1RCPfQvgXTJF97c3VIlUGYR8QlJ98Q+PjvQJtKmoMVtkXZsp0eoUBv+0Qko199CcOarO4pWGlUbV/8vAaLzL0SLofjYn3BVKM01iuAdUnkobJptGf04=', N'ca1fc996-3328-4e07-b97f-9d664d58901d', N'abdullairajudeen@gmail.com', N'9539391527', NULL, NULL, NULL, NULL, 0, CAST(0x0000ACEE0073B6FC AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Subscriber] OFF
SET IDENTITY_INSERT [dbo].[Taluk] ON 

INSERT [dbo].[Taluk] ([TalukID], [DistrictID], [TalukName], [Status]) VALUES (1, 1, N'Neyyathinkara', 0)
SET IDENTITY_INSERT [dbo].[Taluk] OFF
SET IDENTITY_INSERT [dbo].[Team] ON 

INSERT [dbo].[Team] ([TeamId], [TeamUID], [Name], [Description], [TeamLead], [CreatedDate], [Status]) VALUES (1, N'1d3cc6de-cb57-40f8-a86f-681431a68806', N'hai', N'aaa', N'aaaeeee', CAST(0x0000ACEE0073B6FC AS DateTime), 1)
INSERT [dbo].[Team] ([TeamId], [TeamUID], [Name], [Description], [TeamLead], [CreatedDate], [Status]) VALUES (4, N'1d3cc6de-cb57-40f8-a86f-681481a68806', N'hello', N'nnnnnnn', N'wwww', CAST(0x0000ACCC00000000 AS DateTime), 0)
INSERT [dbo].[Team] ([TeamId], [TeamUID], [Name], [Description], [TeamLead], [CreatedDate], [Status]) VALUES (14, N'dd236308-a593-4ae4-b35c-f4dee2508f0c', N'aa', N'aa', N'aa', CAST(0x0000ACF00073A7FF AS DateTime), 1)
INSERT [dbo].[Team] ([TeamId], [TeamUID], [Name], [Description], [TeamLead], [CreatedDate], [Status]) VALUES (15, N'55315288-2dee-43ad-b701-9709c1126c47', N'Test Team 1', N'T', N'T', CAST(0x0000ACF3008A8F4A AS DateTime), 0)
INSERT [dbo].[Team] ([TeamId], [TeamUID], [Name], [Description], [TeamLead], [CreatedDate], [Status]) VALUES (16, N'41b9c974-6257-4830-911d-bcb60066c722', N'Test Team 3', N'T', N'T', CAST(0x0000ACF3008A9E86 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Team] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [Username], [Name], [PasswordHash], [PasswordSalt], [Email], [Status], [RoleType], [CreatedDate], [ResetCode], [ProfilePic]) VALUES (1, N'admin', N'abc', N'oKdNNuagsmG28o7evjjgPP5Nj6zOGs95wtok/aNwPnix9Wi4dhuRU+hD1RCPfQvgXTJF97c3VIlUGYR8QlJ98Q+PjvQJtKmoMVtkXZsp0eoUBv+0Qko199CcOarO4pWGlUbV/8vAaLzL0SLofjYn3BVKM01iuAdUnkobJptGf04=', N'ca1fc996-3328-4e07-b97f-9d664d58901d', N'ab2gmail.com', 0, 0, CAST(0x0000ACB200000000 AS DateTime), N'111', N'avv')
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[Advertisement]  WITH CHECK ADD  CONSTRAINT [FK_Advertisement_AdvertisementPosition] FOREIGN KEY([AdvertismentPositionID])
REFERENCES [dbo].[AdvertisementPosition] ([AdvertismentPositionID])
GO
ALTER TABLE [dbo].[Advertisement] CHECK CONSTRAINT [FK_Advertisement_AdvertisementPosition]
GO
ALTER TABLE [dbo].[AdvertisementShowing]  WITH CHECK ADD  CONSTRAINT [FK_AdvertismentShowing_Adverisment] FOREIGN KEY([AdsID])
REFERENCES [dbo].[Advertisement] ([AdsID])
GO
ALTER TABLE [dbo].[AdvertisementShowing] CHECK CONSTRAINT [FK_AdvertismentShowing_Adverisment]
GO
ALTER TABLE [dbo].[CouponType]  WITH CHECK ADD  CONSTRAINT [FK_BaseCoupon_Layer] FOREIGN KEY([MerchantLayerID])
REFERENCES [dbo].[MerchantLayer] ([LayerID])
GO
ALTER TABLE [dbo].[CouponType] CHECK CONSTRAINT [FK_BaseCoupon_Layer]
GO
ALTER TABLE [dbo].[District]  WITH CHECK ADD  CONSTRAINT [FK_District_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([StateId])
GO
ALTER TABLE [dbo].[District] CHECK CONSTRAINT [FK_District_State]
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_Event_District] FOREIGN KEY([DistrictID])
REFERENCES [dbo].[District] ([DistrictID])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_Event_District]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_Log] FOREIGN KEY([LogID])
REFERENCES [dbo].[Log] ([LogID])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_Log]
GO
ALTER TABLE [dbo].[GameQuestion]  WITH CHECK ADD  CONSTRAINT [FK_GameQuestion_Game] FOREIGN KEY([GameId])
REFERENCES [dbo].[Game] ([GameId])
GO
ALTER TABLE [dbo].[GameQuestion] CHECK CONSTRAINT [FK_GameQuestion_Game]
GO
ALTER TABLE [dbo].[GameQuestion]  WITH CHECK ADD  CONSTRAINT [FK_GameQuestion_Question] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([QuestionId])
GO
ALTER TABLE [dbo].[GameQuestion] CHECK CONSTRAINT [FK_GameQuestion_Question]
GO
ALTER TABLE [dbo].[GameQuestion]  WITH CHECK ADD  CONSTRAINT [FK_GameQuestion_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[GameQuestion] CHECK CONSTRAINT [FK_GameQuestion_User]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_Game] FOREIGN KEY([GameId])
REFERENCES [dbo].[Game] ([GameId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_Game]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_GameQuestion] FOREIGN KEY([GameQuestionId])
REFERENCES [dbo].[GameQuestion] ([GameQuestionId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_GameQuestion]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_QuestionAnswer] FOREIGN KEY([QuestionAnswerId])
REFERENCES [dbo].[QuestionAnswer] ([QuestionAnswerId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_QuestionAnswer]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_Subscriber]
GO
ALTER TABLE [dbo].[MerchantCoupon]  WITH CHECK ADD  CONSTRAINT [FK_Coupon_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchantCoupon] CHECK CONSTRAINT [FK_Coupon_Merchent]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_District] FOREIGN KEY([DistrictID])
REFERENCES [dbo].[District] ([DistrictID])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_District]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_Layer] FOREIGN KEY([LayerID])
REFERENCES [dbo].[MerchantLayer] ([LayerID])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_Layer]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_MerchantType] FOREIGN KEY([MerchantTypeId])
REFERENCES [dbo].[MerchantType] ([MerchantTypeId])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_MerchantType]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_MerchentDetails] FOREIGN KEY([MerchentDetailsID])
REFERENCES [dbo].[MerchentDetails] ([MerchentDetailsID])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_MerchentDetails]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([StateId])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_State]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_Taluk] FOREIGN KEY([TalukID])
REFERENCES [dbo].[Taluk] ([TalukID])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_Taluk]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_Unit] FOREIGN KEY([UnitId])
REFERENCES [dbo].[MerchentUnit] ([MerchentUnitD])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_Unit]
GO
ALTER TABLE [dbo].[MerchentCouponTransaction]  WITH CHECK ADD  CONSTRAINT [FK_MerchentSubscriberTransaction_MerchentTransaction] FOREIGN KEY([MerchentTranscationID])
REFERENCES [dbo].[MerchentTransaction] ([MerchentTranscationID])
GO
ALTER TABLE [dbo].[MerchentCouponTransaction] CHECK CONSTRAINT [FK_MerchentSubscriberTransaction_MerchentTransaction]
GO
ALTER TABLE [dbo].[MerchentDetails]  WITH CHECK ADD  CONSTRAINT [FK_MerchentDetails_Unit] FOREIGN KEY([UnitID])
REFERENCES [dbo].[MerchentUnit] ([MerchentUnitD])
GO
ALTER TABLE [dbo].[MerchentDetails] CHECK CONSTRAINT [FK_MerchentDetails_Unit]
GO
ALTER TABLE [dbo].[MerchentDocumnt]  WITH CHECK ADD  CONSTRAINT [FK_MerchentDocumnt_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentDocumnt] CHECK CONSTRAINT [FK_MerchentDocumnt_Merchent]
GO
ALTER TABLE [dbo].[MerchentFundRequest]  WITH CHECK ADD  CONSTRAINT [FK_MerchentFundRequest_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentFundRequest] CHECK CONSTRAINT [FK_MerchentFundRequest_Merchent]
GO
ALTER TABLE [dbo].[MerchentFundRequest]  WITH CHECK ADD  CONSTRAINT [FK_MerchentFundRequest_User] FOREIGN KEY([ApprovedBy])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[MerchentFundRequest] CHECK CONSTRAINT [FK_MerchentFundRequest_User]
GO
ALTER TABLE [dbo].[MerchentSubscription]  WITH CHECK ADD  CONSTRAINT [FK_MerchentSubscription_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentSubscription] CHECK CONSTRAINT [FK_MerchentSubscription_Merchent]
GO
ALTER TABLE [dbo].[MerchentTransaction]  WITH CHECK ADD  CONSTRAINT [FK_MerchentTransaction_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentTransaction] CHECK CONSTRAINT [FK_MerchentTransaction_Merchent]
GO
ALTER TABLE [dbo].[MerchentUnit]  WITH CHECK ADD  CONSTRAINT [FK_Unit_Taluk] FOREIGN KEY([TalukID])
REFERENCES [dbo].[Taluk] ([TalukID])
GO
ALTER TABLE [dbo].[MerchentUnit] CHECK CONSTRAINT [FK_Unit_Taluk]
GO
ALTER TABLE [dbo].[MerchentVoucher]  WITH CHECK ADD  CONSTRAINT [FK_MerchentVoucher_Coupon] FOREIGN KEY([MerchantCouponId])
REFERENCES [dbo].[MerchantCoupon] ([MerchantCouponId])
GO
ALTER TABLE [dbo].[MerchentVoucher] CHECK CONSTRAINT [FK_MerchentVoucher_Coupon]
GO
ALTER TABLE [dbo].[MerchentVoucher]  WITH CHECK ADD  CONSTRAINT [FK_MerchentVoucher_Subscriber] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentVoucher] CHECK CONSTRAINT [FK_MerchentVoucher_Subscriber]
GO
ALTER TABLE [dbo].[MerchentWallet]  WITH CHECK ADD  CONSTRAINT [FK_MercehntWallet_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentWallet] CHECK CONSTRAINT [FK_MercehntWallet_Merchent]
GO
ALTER TABLE [dbo].[ProgramDetail]  WITH CHECK ADD  CONSTRAINT [FK_ProgramDetail_ProgramMaster] FOREIGN KEY([ProgramMasterId])
REFERENCES [dbo].[ProgramMaster] ([ProgramMasterId])
GO
ALTER TABLE [dbo].[ProgramDetail] CHECK CONSTRAINT [FK_ProgramDetail_ProgramMaster]
GO
ALTER TABLE [dbo].[ProgramParticipiant]  WITH CHECK ADD  CONSTRAINT [FK_ProgramParticipiant_ProgramDetail] FOREIGN KEY([ProgramDetailsId])
REFERENCES [dbo].[ProgramDetail] ([ProgramDetailsId])
GO
ALTER TABLE [dbo].[ProgramParticipiant] CHECK CONSTRAINT [FK_ProgramParticipiant_ProgramDetail]
GO
ALTER TABLE [dbo].[ProgramParticipiant]  WITH CHECK ADD  CONSTRAINT [FK_ProgramParticipiant_Team] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Team] ([TeamId])
GO
ALTER TABLE [dbo].[ProgramParticipiant] CHECK CONSTRAINT [FK_ProgramParticipiant_Team]
GO
ALTER TABLE [dbo].[ProgramScore]  WITH CHECK ADD  CONSTRAINT [FK_ProgramScore_ProgramParticipiant] FOREIGN KEY([ProgramParticipiantId])
REFERENCES [dbo].[ProgramParticipiant] ([ProgramParticipiantId])
GO
ALTER TABLE [dbo].[ProgramScore] CHECK CONSTRAINT [FK_ProgramScore_ProgramParticipiant]
GO
ALTER TABLE [dbo].[ProgramScore]  WITH CHECK ADD  CONSTRAINT [FK_ProgramScore_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[ProgramScore] CHECK CONSTRAINT [FK_ProgramScore_Subscriber]
GO
ALTER TABLE [dbo].[QuestionAnswer]  WITH CHECK ADD  CONSTRAINT [FK_QuestionAnswer_Question] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([QuestionId])
GO
ALTER TABLE [dbo].[QuestionAnswer] CHECK CONSTRAINT [FK_QuestionAnswer_Question]
GO
ALTER TABLE [dbo].[State]  WITH CHECK ADD  CONSTRAINT [FK_State_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[State] CHECK CONSTRAINT [FK_State_Country]
GO
ALTER TABLE [dbo].[Subscriber]  WITH CHECK ADD  CONSTRAINT [FK_Subscriber_SubscriberDetails] FOREIGN KEY([SubsDetailsID])
REFERENCES [dbo].[SubscriberDetails] ([SubsDetailsID])
GO
ALTER TABLE [dbo].[Subscriber] CHECK CONSTRAINT [FK_Subscriber_SubscriberDetails]
GO
ALTER TABLE [dbo].[Subscriber]  WITH CHECK ADD  CONSTRAINT [FK_Subscriber_Taluk] FOREIGN KEY([TalukID])
REFERENCES [dbo].[Taluk] ([TalukID])
GO
ALTER TABLE [dbo].[Subscriber] CHECK CONSTRAINT [FK_Subscriber_Taluk]
GO
ALTER TABLE [dbo].[SubscriberVoucher]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberVoucher_Coupon] FOREIGN KEY([MerchantCouponId])
REFERENCES [dbo].[MerchantCoupon] ([MerchantCouponId])
GO
ALTER TABLE [dbo].[SubscriberVoucher] CHECK CONSTRAINT [FK_SubscriberVoucher_Coupon]
GO
ALTER TABLE [dbo].[SubscriberVoucher]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberVoucher_Subscriber] FOREIGN KEY([SubscriberID])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[SubscriberVoucher] CHECK CONSTRAINT [FK_SubscriberVoucher_Subscriber]
GO
ALTER TABLE [dbo].[SubscriberWallet]  WITH CHECK ADD  CONSTRAINT [FK_Wallet_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[SubscriberWallet] CHECK CONSTRAINT [FK_Wallet_Subscriber]
GO
ALTER TABLE [dbo].[Taluk]  WITH CHECK ADD  CONSTRAINT [FK_Taluk_District] FOREIGN KEY([DistrictID])
REFERENCES [dbo].[District] ([DistrictID])
GO
ALTER TABLE [dbo].[Taluk] CHECK CONSTRAINT [FK_Taluk_District]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Subscriber] FOREIGN KEY([SubscriberID])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Subscriber]
GO
USE [master]
GO
ALTER DATABASE [QuestForBest] SET  READ_WRITE 
GO
