USE [master]
GO
/****** Object:  Database [MoneyDrops]    Script Date: 17-02-2021 12.02.54 PM ******/
CREATE DATABASE [MoneyDrops]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MoneyDrops', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\MoneyDrops.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MoneyDrops_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\MoneyDrops_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MoneyDrops] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MoneyDrops].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MoneyDrops] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MoneyDrops] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MoneyDrops] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MoneyDrops] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MoneyDrops] SET ARITHABORT OFF 
GO
ALTER DATABASE [MoneyDrops] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MoneyDrops] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [MoneyDrops] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MoneyDrops] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MoneyDrops] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MoneyDrops] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MoneyDrops] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MoneyDrops] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MoneyDrops] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MoneyDrops] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MoneyDrops] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MoneyDrops] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MoneyDrops] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MoneyDrops] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MoneyDrops] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MoneyDrops] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MoneyDrops] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MoneyDrops] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MoneyDrops] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MoneyDrops] SET  MULTI_USER 
GO
ALTER DATABASE [MoneyDrops] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MoneyDrops] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MoneyDrops] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MoneyDrops] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [MoneyDrops]
GO
/****** Object:  Table [dbo].[AboutTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AboutTbl](
	[AboutID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[ShortContent] [nvarchar](max) NULL,
	[LongContent] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[SEOKeywords] [nvarchar](max) NULL,
 CONSTRAINT [PK_About] PRIMARY KEY CLUSTERED 
(
	[AboutID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobApplicationTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobApplicationTbl](
	[CareerJobApplicationID] [uniqueidentifier] NOT NULL,
	[CareerJobOfferID] [uniqueidentifier] NULL,
	[Name] [nvarchar](150) NULL,
	[DOB] [datetime] NULL,
	[Gender] [tinyint] NULL,
	[Qualification] [nvarchar](250) NULL,
	[Comments] [nvarchar](max) NULL,
	[CVFilePath] [nvarchar](max) NULL,
	[ApplicationStatus] [tinyint] NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_CareerJobApplication] PRIMARY KEY CLUSTERED 
(
	[CareerJobApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobOfferRequirementTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobOfferRequirementTbl](
	[CareerJobOfferRequirementID] [uniqueidentifier] NOT NULL,
	[CareerJobOfferID] [uniqueidentifier] NULL,
	[Requirement] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_CareerJobOfferRequirementTbl] PRIMARY KEY CLUSTERED 
(
	[CareerJobOfferRequirementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerJobOfferTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerJobOfferTbl](
	[CareerJobOfferID] [uniqueidentifier] NOT NULL,
	[JobTitle] [nvarchar](250) NULL,
	[JobDescription] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[ValidUpto] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_CareerJobOffer] PRIMARY KEY CLUSTERED 
(
	[CareerJobOfferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactTbl](
	[ContactID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[DisplaySequence] [int] NOT NULL,
	[AddressLine1] [nvarchar](250) NOT NULL,
	[AddressLine2] [nvarchar](250) NULL,
	[AddressLine3] [nvarchar](250) NULL,
	[CityID] [uniqueidentifier] NOT NULL,
	[StateID] [uniqueidentifier] NOT NULL,
	[CountryID] [uniqueidentifier] NOT NULL,
	[LocLatitude] [nvarchar](max) NULL,
	[LocLongitude] [nvarchar](max) NULL,
	[Email] [nvarchar](250) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Mobile1] [nvarchar](50) NULL,
	[Mobile2] [nvarchar](50) NULL,
	[RecordStatus] [tinyint] NULL,
	[IsHeadOffice] [bit] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FAQTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQTbl](
	[FaqID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [tinyint] NULL,
	[Question] [nvarchar](max) NULL,
	[Answer] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[FaqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NavigationTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NavigationTbl](
	[NavigationID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[Link] [nvarchar](max) NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_Navigation] PRIMARY KEY CLUSTERED 
(
	[NavigationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTbl](
	[NewsID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Type] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceCategoryTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceCategoryTbl](
	[ServiceCategoryID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_ServiceCategoryTbl] PRIMARY KEY CLUSTERED 
(
	[ServiceCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceSubCategoryTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceSubCategoryTbl](
	[ServiceSubCategoryID] [uniqueidentifier] NOT NULL,
	[ServiceCategoryID] [uniqueidentifier] NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
 CONSTRAINT [PK_ServiceSubCategory] PRIMARY KEY CLUSTERED 
(
	[ServiceSubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceTbl](
	[ServiceID] [uniqueidentifier] NOT NULL,
	[Type] [tinyint] NULL,
	[ServiceCategoryID] [uniqueidentifier] NULL,
	[ServiceSubCategoryID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_Service_1] PRIMARY KEY CLUSTERED 
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SettingsTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingsTbl](
	[SettingsID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[TagLine] [nvarchar](250) NULL,
	[Logo] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[TermsConditions] [nvarchar](max) NULL,
	[FaviconPath] [nvarchar](max) NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[SettingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SliderTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SliderTbl](
	[SliderID] [int] IDENTITY(1,1) NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_SliderTbl] PRIMARY KEY CLUSTERED 
(
	[SliderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SocialMediaTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SocialMediaTbl](
	[SocialMediaID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Type] [tinyint] NULL,
	[IconPath] [nvarchar](max) NULL,
	[Link] [nvarchar](max) NULL,
	[DisplaySequence] [int] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_SocialMedia] PRIMARY KEY CLUSTERED 
(
	[SocialMediaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserTbl]    Script Date: 17-02-2021 12.02.54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTbl](
	[UserID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](150) NULL,
	[Email] [nvarchar](250) NULL,
	[Username] [nvarchar](150) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Type] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[RecordStatus] [tinyint] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[CareerJobApplicationTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobApplicationTbl_CareerJobOfferTbl] FOREIGN KEY([CareerJobOfferID])
REFERENCES [dbo].[CareerJobOfferTbl] ([CareerJobOfferID])
GO
ALTER TABLE [dbo].[CareerJobApplicationTbl] CHECK CONSTRAINT [FK_CareerJobApplicationTbl_CareerJobOfferTbl]
GO
ALTER TABLE [dbo].[CareerJobOfferRequirementTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobOfferRequirementTbl_CareerJobOfferTbl] FOREIGN KEY([CareerJobOfferID])
REFERENCES [dbo].[CareerJobOfferTbl] ([CareerJobOfferID])
GO
ALTER TABLE [dbo].[CareerJobOfferRequirementTbl] CHECK CONSTRAINT [FK_CareerJobOfferRequirementTbl_CareerJobOfferTbl]
GO
ALTER TABLE [dbo].[CareerJobOfferTbl]  WITH CHECK ADD  CONSTRAINT [FK_CareerJobOfferTbl_UserTbl] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTbl] ([UserID])
GO
ALTER TABLE [dbo].[CareerJobOfferTbl] CHECK CONSTRAINT [FK_CareerJobOfferTbl_UserTbl]
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceCategoryTbl] FOREIGN KEY([ServiceCategoryID])
REFERENCES [dbo].[ServiceCategoryTbl] ([ServiceCategoryID])
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl] CHECK CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceCategoryTbl]
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceSubCategoryTbl] FOREIGN KEY([ServiceSubCategoryID])
REFERENCES [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID])
GO
ALTER TABLE [dbo].[ServiceSubCategoryTbl] CHECK CONSTRAINT [FK_ServiceSubCategoryTbl_ServiceSubCategoryTbl]
GO
ALTER TABLE [dbo].[ServiceTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTbl_ServiceCategoryTbl] FOREIGN KEY([ServiceSubCategoryID])
REFERENCES [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID])
GO
ALTER TABLE [dbo].[ServiceTbl] CHECK CONSTRAINT [FK_ServiceTbl_ServiceCategoryTbl]
GO
ALTER TABLE [dbo].[ServiceTbl]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTbl_ServiceSubCategoryTbl] FOREIGN KEY([ServiceSubCategoryID])
REFERENCES [dbo].[ServiceSubCategoryTbl] ([ServiceSubCategoryID])
GO
ALTER TABLE [dbo].[ServiceTbl] CHECK CONSTRAINT [FK_ServiceTbl_ServiceSubCategoryTbl]
GO
USE [master]
GO
ALTER DATABASE [MoneyDrops] SET  READ_WRITE 
GO
