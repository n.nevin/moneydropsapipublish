USE [master]
GO
/****** Object:  Database [QuestForBest]    Script Date: 17-03-2021 10:32:44 ******/
CREATE DATABASE [QuestForBest]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuestForBest', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.HACKBALSERVER\MSSQL\DATA\QuestForBest.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QuestForBest_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.HACKBALSERVER\MSSQL\DATA\QuestForBest_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [QuestForBest] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuestForBest].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuestForBest] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuestForBest] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuestForBest] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuestForBest] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuestForBest] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuestForBest] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuestForBest] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuestForBest] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuestForBest] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuestForBest] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuestForBest] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuestForBest] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuestForBest] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuestForBest] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuestForBest] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QuestForBest] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuestForBest] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuestForBest] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuestForBest] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuestForBest] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuestForBest] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuestForBest] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuestForBest] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QuestForBest] SET  MULTI_USER 
GO
ALTER DATABASE [QuestForBest] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuestForBest] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuestForBest] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuestForBest] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QuestForBest] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [QuestForBest] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [QuestForBest] SET QUERY_STORE = OFF
GO
USE [QuestForBest]
GO
/****** Object:  Table [dbo].[Adverisment]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adverisment](
	[AdsID] [int] IDENTITY(1,1) NOT NULL,
	[Tittle] [nvarchar](50) NOT NULL,
	[SecondTitle] [nvarchar](50) NOT NULL,
	[Image] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ShortDescription] [nvarchar](50) NOT NULL,
	[AdvertismentPositionID] [int] NOT NULL,
	[Entredon] [date] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_Adverisment] PRIMARY KEY CLUSTERED 
(
	[AdsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdverismentPosition]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdverismentPosition](
	[AdvertismentPositionID] [int] NOT NULL,
	[PageTitle] [nvarchar](50) NOT NULL,
	[Position] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AdverismentPosition] PRIMARY KEY CLUSTERED 
(
	[AdvertismentPositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdvertismentShowing]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvertismentShowing](
	[AdsShowID] [int] IDENTITY(1,1) NOT NULL,
	[AdsID] [int] NOT NULL,
	[AdsPositionID] [int] NOT NULL,
	[ExpieredOn] [date] NOT NULL,
	[SponserdBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AdvertismentShowing] PRIMARY KEY CLUSTERED 
(
	[AdsShowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BaseCoupon]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaseCoupon](
	[BaseCouponID] [bigint] IDENTITY(1,1) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[LayerID] [int] NOT NULL,
 CONSTRAINT [PK_BaseCoupon] PRIMARY KEY CLUSTERED 
(
	[BaseCouponID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](350) NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](15) NULL,
	[LicenceType] [tinyint] NOT NULL,
	[DeviceLimit] [int] NULL,
	[ImagePath] [nvarchar](100) NULL,
	[MailStatus] [bit] NOT NULL,
	[RecordStatus] [tinyint] NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Coupon]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coupon](
	[CouponID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
	[CouponName] [nvarchar](50) NOT NULL,
	[BaseCouponID] [bigint] NOT NULL,
	[CouponCount] [int] NOT NULL,
	[ValidOn] [date] NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Coupon] PRIMARY KEY CLUSTERED 
(
	[CouponID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[District]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[DistrictID] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[DistrictID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Image] [nvarchar](50) NOT NULL,
	[EventDate] [date] NOT NULL,
	[Venue] [nvarchar](50) NOT NULL,
	[DistrictID] [int] NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[isActive] [bit] NOT NULL,
	[EventCreatedBy] [nvarchar](50) NOT NULL,
	[SponcerdBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Game]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Game](
	[GameId] [bigint] IDENTITY(1,1) NOT NULL,
	[GameUID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[PublsihOn] [date] NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[Active] [bit] NOT NULL,
	[iSLogin] [bit] NOT NULL,
	[LogID] [int] NOT NULL,
	[SponserdBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[GameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameQuestion]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameQuestion](
	[GameQuestionId] [bigint] IDENTITY(1,1) NOT NULL,
	[GameQuestionUID] [uniqueidentifier] NOT NULL,
	[GameId] [bigint] NOT NULL,
	[QuestionId] [bigint] NOT NULL,
	[PublishedStatus] [tinyint] NOT NULL,
	[PublishedTime] [datetime] NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_GameQuestion] PRIMARY KEY CLUSTERED 
(
	[GameQuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameResult]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameResult](
	[GameResultId] [bigint] IDENTITY(1,1) NOT NULL,
	[GameResultUID] [uniqueidentifier] NOT NULL,
	[SubscriberId] [bigint] NOT NULL,
	[GameId] [bigint] NOT NULL,
	[GameQuestionId] [bigint] NOT NULL,
	[QuestionAnswerId] [bigint] NOT NULL,
	[IsCorrectAnswer] [bit] NOT NULL,
	[DisplayTime] [datetime] NOT NULL,
	[ResponseTime] [datetime] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_GameResult] PRIMARY KEY CLUSTERED 
(
	[GameResultId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Layer]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Layer](
	[LayerID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Layer] PRIMARY KEY CLUSTERED 
(
	[LayerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[AdsQues] [nvarchar](250) NOT NULL,
	[AdsAns] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Merchent]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Merchent](
	[MerchentID] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchentDetailsID] [int] NULL,
	[UniqueID] [varchar](50) NOT NULL,
	[LayerID] [int] NOT NULL,
	[MerchentName] [varchar](500) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Email] [nvarchar](150) NULL,
	[MobileNo] [nvarchar](15) NULL,
	[Address] [nvarchar](250) NULL,
	[Pincode] [nvarchar](10) NULL,
	[isVerify] [bit] NOT NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_Merchent] PRIMARY KEY CLUSTERED 
(
	[MerchentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MerchentDetails]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentDetails](
	[MerchentDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentCode] [int] NOT NULL,
	[Unit] [nvarchar](50) NOT NULL,
	[DistrictID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MerchentDetails] PRIMARY KEY CLUSTERED 
(
	[MerchentDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MerchentDocumnt]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentDocumnt](
	[MerchentDocumentID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
	[DocumentType] [nvarchar](50) NOT NULL,
	[Path] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_MerchentDocumnt] PRIMARY KEY CLUSTERED 
(
	[MerchentDocumentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MerchentFundRequest]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentFundRequest](
	[FundRequestID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NULL,
	[TransactionUniqID] [varchar](50) NULL,
	[BankName] [nvarchar](50) NOT NULL,
	[IFSCCode] [varchar](50) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[TransferDate] [date] NOT NULL,
	[IsApprove] [bit] NOT NULL,
 CONSTRAINT [PK_MerchentFundRequest] PRIMARY KEY CLUSTERED 
(
	[FundRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MerchentSubscriberTransaction]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentSubscriberTransaction](
	[MerchentSubstransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentTranscationID] [int] NOT NULL,
	[TransactionOn] [date] NOT NULL,
	[Amount] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_MerchentSubscriberTransaction] PRIMARY KEY CLUSTERED 
(
	[MerchentSubstransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MerchentSubscription]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentSubscription](
	[MerchentSubscriptionID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
 CONSTRAINT [PK_MerchentSubscription] PRIMARY KEY CLUSTERED 
(
	[MerchentSubscriptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MerchentTransaction]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentTransaction](
	[MerchentTranscationID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
	[TransactionID] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [tinyint] NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_MerchentTransaction] PRIMARY KEY CLUSTERED 
(
	[MerchentTranscationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MerchentWallet]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchentWallet](
	[MerchentWalletID] [int] IDENTITY(1,1) NOT NULL,
	[MerchentID] [bigint] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_MercehntWallet] PRIMARY KEY CLUSTERED 
(
	[MerchentWalletID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationID] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[UpdatedOn] [date] NOT NULL,
	[EntredOn] [date] NOT NULL,
	[Url] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[QuestionId] [bigint] IDENTITY(1,1) NOT NULL,
	[QuestionUID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Type] [tinyint] NOT NULL,
	[Path] [nvarchar](500) NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[SponserdBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionAnswer]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionAnswer](
	[QuestionAnswerId] [bigint] IDENTITY(1,1) NOT NULL,
	[QuestionAnswerUID] [uniqueidentifier] NOT NULL,
	[QuestionId] [bigint] NOT NULL,
	[Choice] [nvarchar](250) NOT NULL,
	[Path] [nvarchar](500) NULL,
	[Type] [tinyint] NOT NULL,
	[IsAnswer] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_QuestionAnswer] PRIMARY KEY CLUSTERED 
(
	[QuestionAnswerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sponser]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sponser](
	[SponserID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Logo] [nvarchar](150) NOT NULL,
	[Text] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
	[ShortDescription] [nvarchar](550) NOT NULL,
 CONSTRAINT [PK_Sponser] PRIMARY KEY CLUSTERED 
(
	[SponserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subscriber]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subscriber](
	[SubscriberId] [bigint] IDENTITY(1,1) NOT NULL,
	[SubscriberUID] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](15) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Username] [nvarchar](150) NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Email] [nvarchar](150) NULL,
	[MobileNo] [nvarchar](15) NOT NULL,
	[AadhaarNo] [nvarchar](30) NULL,
	[Address] [nvarchar](250) NULL,
	[Pincode] [nvarchar](10) NULL,
	[DistrictId] [int] NOT NULL,
	[SubscriberStatus] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[SubsUniqueID] [int] NULL,
	[EmailVerifictaionCode] [nvarchar](50) NULL,
	[EmailIsVerify] [bit] NULL,
	[PhoneVerificationcode] [nvarchar](50) NULL,
	[PhoneIsVerify] [bit] NULL,
	[PasswordResetCode] [nvarchar](50) NULL,
	[ProfilePic] [nvarchar](50) NULL,
	[SubsDetailsID] [int] NULL,
	[isVerify] [bit] NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_Subscriber] PRIMARY KEY CLUSTERED 
(
	[SubscriberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscriberDetails]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberDetails](
	[SubsDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[SubsCode] [int] NOT NULL,
	[SetBoxBarCode] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](250) NOT NULL,
	[MobileNo] [nvarchar](20) NOT NULL,
	[District] [nvarchar](50) NOT NULL,
	[OperatorCode] [int] NOT NULL,
 CONSTRAINT [PK_SubscriberDetails] PRIMARY KEY CLUSTERED 
(
	[SubsDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscriberVoucher]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberVoucher](
	[SubsVoucherID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberID] [bigint] NOT NULL,
	[CouponID] [int] NOT NULL,
	[ExperedOn] [date] NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[isRedeemed] [bit] NOT NULL,
	[CouponUniqueID] [int] NOT NULL,
 CONSTRAINT [PK_SubscriberVoucher] PRIMARY KEY CLUSTERED 
(
	[SubsVoucherID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscriptionPackage]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriptionPackage](
	[SubscriptionPackageID] [int] IDENTITY(1,1) NOT NULL,
	[ExpiredOn] [date] NOT NULL,
	[SubscriptionPackageName] [nvarchar](50) NOT NULL,
	[Validity] [date] NOT NULL,
	[Amount] [int] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_SubscriptionPackage] PRIMARY KEY CLUSTERED 
(
	[SubscriptionPackageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberID] [bigint] NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Amount] [int] NOT NULL,
	[UpdtedOn] [date] NOT NULL,
	[CreatedOn] [date] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PasswordSalt] [uniqueidentifier] NULL,
	[Email] [nvarchar](100) NULL,
	[Status] [tinyint] NOT NULL,
	[RoleType] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ResetCode] [nvarchar](100) NOT NULL,
	[ProfilePic] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Wallet]    Script Date: 17-03-2021 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallet](
	[walletID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberId] [bigint] NOT NULL,
	[Amount] [int] NOT NULL,
	[UpdatedOn] [date] NOT NULL,
 CONSTRAINT [PK_Wallet] PRIMARY KEY CLUSTERED 
(
	[walletID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BaseCoupon] ON 

INSERT [dbo].[BaseCoupon] ([BaseCouponID], [Amount], [LayerID]) VALUES (1, CAST(5000.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[BaseCoupon] ([BaseCouponID], [Amount], [LayerID]) VALUES (2, CAST(500.00 AS Decimal(18, 2)), 2)
SET IDENTITY_INSERT [dbo].[BaseCoupon] OFF
GO
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([CountryId], [Code], [Name], [Status]) VALUES (3, N'IN', N'India', 0)
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET IDENTITY_INSERT [dbo].[Coupon] ON 

INSERT [dbo].[Coupon] ([CouponID], [MerchentID], [CouponName], [BaseCouponID], [CouponCount], [ValidOn], [ExpiredOn], [IsActive]) VALUES (1, 2, N'Tets Copon', 1, 2, CAST(N'2021-03-20' AS Date), CAST(N'2021-03-31' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Coupon] OFF
GO
SET IDENTITY_INSERT [dbo].[District] ON 

INSERT [dbo].[District] ([DistrictID], [StateId], [Name], [Status]) VALUES (1, 2, N'Kollam', 0)
SET IDENTITY_INSERT [dbo].[District] OFF
GO
SET IDENTITY_INSERT [dbo].[Layer] ON 

INSERT [dbo].[Layer] ([LayerID], [Type]) VALUES (1, N'Advertisement')
INSERT [dbo].[Layer] ([LayerID], [Type]) VALUES (2, N'No Advertisement')
SET IDENTITY_INSERT [dbo].[Layer] OFF
GO
SET IDENTITY_INSERT [dbo].[Merchent] ON 

INSERT [dbo].[Merchent] ([MerchentID], [MerchentDetailsID], [UniqueID], [LayerID], [MerchentName], [PasswordHash], [PasswordSalt], [Email], [MobileNo], [Address], [Pincode], [isVerify], [isActive]) VALUES (2, 4, N'MER', 1, N'Aspirom', N'oKdNNuagsmG28o7evjjgPP5Nj6zOGs95wtok/aNwPnix9Wi4dhuRU+hD1RCPfQvgXTJF97c3VIlUGYR8QlJ98Q+PjvQJtKmoMVtkXZsp0eoUBv+0Qko199CcOarO4pWGlUbV/8vAaLzL0SLofjYn3BVKM01iuAdUnkobJptGf04=', N'ca1fc996-3328-4e07-b97f-9d664d58901d', N'abdu@gmail.com', N'9539391527', N'Sabisa', N'91535', 0, 0)
SET IDENTITY_INSERT [dbo].[Merchent] OFF
GO
SET IDENTITY_INSERT [dbo].[MerchentDetails] ON 

INSERT [dbo].[MerchentDetails] ([MerchentDetailsID], [MerchentCode], [Unit], [DistrictID], [Name], [MobileNo], [Email]) VALUES (4, 1, N'1', 1, N'1', N'1', N'1')
SET IDENTITY_INSERT [dbo].[MerchentDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[MerchentFundRequest] ON 

INSERT [dbo].[MerchentFundRequest] ([FundRequestID], [MerchentID], [TransactionUniqID], [BankName], [IFSCCode], [Amount], [TransferDate], [IsApprove]) VALUES (1, 2, N'658-', N'State Bank of Trancve', N'IFSC', CAST(522.00 AS Decimal(18, 2)), CAST(N'2021-03-19' AS Date), 0)
SET IDENTITY_INSERT [dbo].[MerchentFundRequest] OFF
GO
SET IDENTITY_INSERT [dbo].[State] ON 

INSERT [dbo].[State] ([StateId], [CountryId], [Name], [Code], [Status]) VALUES (2, 3, N'Kerala', N'KL', 0)
SET IDENTITY_INSERT [dbo].[State] OFF
GO
ALTER TABLE [dbo].[AdvertismentShowing]  WITH CHECK ADD  CONSTRAINT [FK_AdvertismentShowing_Adverisment] FOREIGN KEY([AdsID])
REFERENCES [dbo].[Adverisment] ([AdsID])
GO
ALTER TABLE [dbo].[AdvertismentShowing] CHECK CONSTRAINT [FK_AdvertismentShowing_Adverisment]
GO
ALTER TABLE [dbo].[AdvertismentShowing]  WITH CHECK ADD  CONSTRAINT [FK_AdvertismentShowing_AdverismentPosition] FOREIGN KEY([AdsPositionID])
REFERENCES [dbo].[AdverismentPosition] ([AdvertismentPositionID])
GO
ALTER TABLE [dbo].[AdvertismentShowing] CHECK CONSTRAINT [FK_AdvertismentShowing_AdverismentPosition]
GO
ALTER TABLE [dbo].[BaseCoupon]  WITH CHECK ADD  CONSTRAINT [FK_BaseCoupon_Layer] FOREIGN KEY([LayerID])
REFERENCES [dbo].[Layer] ([LayerID])
GO
ALTER TABLE [dbo].[BaseCoupon] CHECK CONSTRAINT [FK_BaseCoupon_Layer]
GO
ALTER TABLE [dbo].[Coupon]  WITH CHECK ADD  CONSTRAINT [FK_Coupon_BaseCoupon] FOREIGN KEY([BaseCouponID])
REFERENCES [dbo].[BaseCoupon] ([BaseCouponID])
GO
ALTER TABLE [dbo].[Coupon] CHECK CONSTRAINT [FK_Coupon_BaseCoupon]
GO
ALTER TABLE [dbo].[Coupon]  WITH CHECK ADD  CONSTRAINT [FK_Coupon_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[Coupon] CHECK CONSTRAINT [FK_Coupon_Merchent]
GO
ALTER TABLE [dbo].[District]  WITH CHECK ADD  CONSTRAINT [FK_District_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([StateId])
GO
ALTER TABLE [dbo].[District] CHECK CONSTRAINT [FK_District_State]
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_Event_District] FOREIGN KEY([DistrictID])
REFERENCES [dbo].[District] ([DistrictID])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_Event_District]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_Log] FOREIGN KEY([LogID])
REFERENCES [dbo].[Log] ([LogID])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_Log]
GO
ALTER TABLE [dbo].[GameQuestion]  WITH CHECK ADD  CONSTRAINT [FK_GameQuestion_Game] FOREIGN KEY([GameId])
REFERENCES [dbo].[Game] ([GameId])
GO
ALTER TABLE [dbo].[GameQuestion] CHECK CONSTRAINT [FK_GameQuestion_Game]
GO
ALTER TABLE [dbo].[GameQuestion]  WITH CHECK ADD  CONSTRAINT [FK_GameQuestion_Question] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([QuestionId])
GO
ALTER TABLE [dbo].[GameQuestion] CHECK CONSTRAINT [FK_GameQuestion_Question]
GO
ALTER TABLE [dbo].[GameQuestion]  WITH CHECK ADD  CONSTRAINT [FK_GameQuestion_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[GameQuestion] CHECK CONSTRAINT [FK_GameQuestion_User]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_Game] FOREIGN KEY([GameId])
REFERENCES [dbo].[Game] ([GameId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_Game]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_GameQuestion] FOREIGN KEY([GameQuestionId])
REFERENCES [dbo].[GameQuestion] ([GameQuestionId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_GameQuestion]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_QuestionAnswer] FOREIGN KEY([QuestionAnswerId])
REFERENCES [dbo].[QuestionAnswer] ([QuestionAnswerId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_QuestionAnswer]
GO
ALTER TABLE [dbo].[GameResult]  WITH CHECK ADD  CONSTRAINT [FK_GameResult_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[GameResult] CHECK CONSTRAINT [FK_GameResult_Subscriber]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_Layer] FOREIGN KEY([LayerID])
REFERENCES [dbo].[Layer] ([LayerID])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_Layer]
GO
ALTER TABLE [dbo].[Merchent]  WITH CHECK ADD  CONSTRAINT [FK_Merchent_MerchentDetails] FOREIGN KEY([MerchentDetailsID])
REFERENCES [dbo].[MerchentDetails] ([MerchentDetailsID])
GO
ALTER TABLE [dbo].[Merchent] CHECK CONSTRAINT [FK_Merchent_MerchentDetails]
GO
ALTER TABLE [dbo].[MerchentDetails]  WITH CHECK ADD  CONSTRAINT [FK_MerchentDetails_District] FOREIGN KEY([DistrictID])
REFERENCES [dbo].[District] ([DistrictID])
GO
ALTER TABLE [dbo].[MerchentDetails] CHECK CONSTRAINT [FK_MerchentDetails_District]
GO
ALTER TABLE [dbo].[MerchentDocumnt]  WITH CHECK ADD  CONSTRAINT [FK_MerchentDocumnt_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentDocumnt] CHECK CONSTRAINT [FK_MerchentDocumnt_Merchent]
GO
ALTER TABLE [dbo].[MerchentFundRequest]  WITH CHECK ADD  CONSTRAINT [FK_MerchentFundRequest_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentFundRequest] CHECK CONSTRAINT [FK_MerchentFundRequest_Merchent]
GO
ALTER TABLE [dbo].[MerchentSubscriberTransaction]  WITH CHECK ADD  CONSTRAINT [FK_MerchentSubscriberTransaction_MerchentTransaction] FOREIGN KEY([MerchentTranscationID])
REFERENCES [dbo].[MerchentTransaction] ([MerchentTranscationID])
GO
ALTER TABLE [dbo].[MerchentSubscriberTransaction] CHECK CONSTRAINT [FK_MerchentSubscriberTransaction_MerchentTransaction]
GO
ALTER TABLE [dbo].[MerchentSubscription]  WITH CHECK ADD  CONSTRAINT [FK_MerchentSubscription_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentSubscription] CHECK CONSTRAINT [FK_MerchentSubscription_Merchent]
GO
ALTER TABLE [dbo].[MerchentTransaction]  WITH CHECK ADD  CONSTRAINT [FK_MerchentTransaction_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentTransaction] CHECK CONSTRAINT [FK_MerchentTransaction_Merchent]
GO
ALTER TABLE [dbo].[MerchentWallet]  WITH CHECK ADD  CONSTRAINT [FK_MercehntWallet_Merchent] FOREIGN KEY([MerchentID])
REFERENCES [dbo].[Merchent] ([MerchentID])
GO
ALTER TABLE [dbo].[MerchentWallet] CHECK CONSTRAINT [FK_MercehntWallet_Merchent]
GO
ALTER TABLE [dbo].[QuestionAnswer]  WITH CHECK ADD  CONSTRAINT [FK_QuestionAnswer_Question] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([QuestionId])
GO
ALTER TABLE [dbo].[QuestionAnswer] CHECK CONSTRAINT [FK_QuestionAnswer_Question]
GO
ALTER TABLE [dbo].[State]  WITH CHECK ADD  CONSTRAINT [FK_State_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO
ALTER TABLE [dbo].[State] CHECK CONSTRAINT [FK_State_Country]
GO
ALTER TABLE [dbo].[Subscriber]  WITH CHECK ADD  CONSTRAINT [FK_Subscriber_SubscriberDetails] FOREIGN KEY([SubsDetailsID])
REFERENCES [dbo].[SubscriberDetails] ([SubsDetailsID])
GO
ALTER TABLE [dbo].[Subscriber] CHECK CONSTRAINT [FK_Subscriber_SubscriberDetails]
GO
ALTER TABLE [dbo].[SubscriberVoucher]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberVoucher_Coupon] FOREIGN KEY([CouponID])
REFERENCES [dbo].[Coupon] ([CouponID])
GO
ALTER TABLE [dbo].[SubscriberVoucher] CHECK CONSTRAINT [FK_SubscriberVoucher_Coupon]
GO
ALTER TABLE [dbo].[SubscriberVoucher]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberVoucher_Subscriber] FOREIGN KEY([SubscriberID])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[SubscriberVoucher] CHECK CONSTRAINT [FK_SubscriberVoucher_Subscriber]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Subscriber] FOREIGN KEY([SubscriberID])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Subscriber]
GO
ALTER TABLE [dbo].[Wallet]  WITH CHECK ADD  CONSTRAINT [FK_Wallet_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[Wallet] CHECK CONSTRAINT [FK_Wallet_Subscriber]
GO
USE [master]
GO
ALTER DATABASE [QuestForBest] SET  READ_WRITE 
GO
